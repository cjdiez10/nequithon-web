<div class="companies view">
	<div class="row">
		<div class="col-md-12">
			<div class="page-header">
				<h1><?php echo __('Company'); ?></h1>
			</div>
		</div>
	</div>

	<div class="row">

		<div class="col-md-3">
			<div class="actions">
				<div class="panel panel-default">
					<div class="panel-heading">Actions</div>
						<div class="panel-body">
							<ul class="nav nav-pills nav-stacked">
									<li><?php echo $this->Html->link(__('<span class="glyphicon glyphicon-edit"></span>&nbsp&nbsp;Edit Company'), array('action' => 'edit', $company['Company']['id']), array('escape' => false)); ?> </li>
		<li><?php echo $this->Form->postLink(__('<span class="glyphicon glyphicon-remove"></span>&nbsp;&nbsp;Delete Company'), array('action' => 'delete', $company['Company']['id']), array('escape' => false), __('Are you sure you want to delete # %s?', $company['Company']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('<span class="glyphicon glyphicon-list"></span>&nbsp&nbsp;List Companies'), array('action' => 'index'), array('escape' => false)); ?> </li>
		<li><?php echo $this->Html->link(__('<span class="glyphicon glyphicon-plus"></span>&nbsp&nbsp;New Company'), array('action' => 'add'), array('escape' => false)); ?> </li>
		<li><?php echo $this->Html->link(__('<span class="glyphicon glyphicon-list"></span>&nbsp&nbsp;List Webusers'), array('controller' => 'webusers', 'action' => 'index'), array('escape' => false)); ?> </li>
		<li><?php echo $this->Html->link(__('<span class="glyphicon glyphicon-plus"></span>&nbsp&nbsp;New Webuser'), array('controller' => 'webusers', 'action' => 'add'), array('escape' => false)); ?> </li>
		<li><?php echo $this->Html->link(__('<span class="glyphicon glyphicon-list"></span>&nbsp&nbsp;List Challenges'), array('controller' => 'challenges', 'action' => 'index'), array('escape' => false)); ?> </li>
		<li><?php echo $this->Html->link(__('<span class="glyphicon glyphicon-plus"></span>&nbsp&nbsp;New Challenge'), array('controller' => 'challenges', 'action' => 'add'), array('escape' => false)); ?> </li>
		<li><?php echo $this->Html->link(__('<span class="glyphicon glyphicon-list"></span>&nbsp&nbsp;List Web Users'), array('controller' => 'web_users', 'action' => 'index'), array('escape' => false)); ?> </li>
		<li><?php echo $this->Html->link(__('<span class="glyphicon glyphicon-plus"></span>&nbsp&nbsp;New Web User'), array('controller' => 'web_users', 'action' => 'add'), array('escape' => false)); ?> </li>
							</ul>
						</div><!-- end body -->
				</div><!-- end panel -->
			</div><!-- end actions -->
		</div><!-- end col md 3 -->

		<div class="col-md-9">			
			<table cellpadding="0" cellspacing="0" class="table table-striped">
				<tbody>
				<tr>
		<th><?php echo __('Id'); ?></th>
		<td>
			<?php echo h($company['Company']['id']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Nombre'); ?></th>
		<td>
			<?php echo h($company['Company']['nombre']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Webuser'); ?></th>
		<td>
			<?php echo $this->Html->link($company['Webuser']['id'], array('controller' => 'webusers', 'action' => 'view', $company['Webuser']['id'])); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Direccion'); ?></th>
		<td>
			<?php echo h($company['Company']['direccion']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Imagen'); ?></th>
		<td>
			<?php echo h($company['Company']['imagen']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Created'); ?></th>
		<td>
			<?php echo h($company['Company']['created']); ?>
			&nbsp;
		</td>
</tr>
				</tbody>
			</table>

		</div><!-- end col md 9 -->

	</div>
</div>

<div class="related row">
	<div class="col-md-12">
	<h3><?php echo __('Related Challenges'); ?></h3>
	<?php if (!empty($company['Challenge'])): ?>
	<table cellpadding = "0" cellspacing = "0" class="table table-striped">
	<thead>
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Nombre'); ?></th>
		<th><?php echo __('Fecha'); ?></th>
		<th><?php echo __('Descripcion'); ?></th>
		<th><?php echo __('Imagen'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Company Id'); ?></th>
		<th class="actions"></th>
	</tr>
	<thead>
	<tbody>
	<?php foreach ($company['Challenge'] as $challenge): ?>
		<tr>
			<td><?php echo $challenge['id']; ?></td>
			<td><?php echo $challenge['nombre']; ?></td>
			<td><?php echo $challenge['fecha']; ?></td>
			<td><?php echo $challenge['descripcion']; ?></td>
			<td><?php echo $challenge['imagen']; ?></td>
			<td><?php echo $challenge['created']; ?></td>
			<td><?php echo $challenge['company_id']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('<span class="glyphicon glyphicon-search"></span>'), array('controller' => 'challenges', 'action' => 'view', $challenge['id']), array('escape' => false)); ?>
				<?php echo $this->Html->link(__('<span class="glyphicon glyphicon-edit"></span>'), array('controller' => 'challenges', 'action' => 'edit', $challenge['id']), array('escape' => false)); ?>
				<?php echo $this->Form->postLink(__('<span class="glyphicon glyphicon-remove"></span>'), array('controller' => 'challenges', 'action' => 'delete', $challenge['id']), array('escape' => false), __('Are you sure you want to delete # %s?', $challenge['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</tbody>
	</table>
<?php endif; ?>

	<div class="actions">
		<?php echo $this->Html->link(__('<span class="glyphicon glyphicon-plus"></span>&nbsp;&nbsp;New Challenge'), array('controller' => 'challenges', 'action' => 'add'), array('escape' => false, 'class' => 'btn btn-default')); ?> 
	</div>
	</div><!-- end col md 12 -->
</div>
<div class="related row">
	<div class="col-md-12">
	<h3><?php echo __('Related Web Users'); ?></h3>
	<?php if (!empty($company['WebUser'])): ?>
	<table cellpadding = "0" cellspacing = "0" class="table table-striped">
	<thead>
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Nombre'); ?></th>
		<th><?php echo __('Usuario'); ?></th>
		<th><?php echo __('Password'); ?></th>
		<th><?php echo __('Copiapassword'); ?></th>
		<th><?php echo __('Webuser Profile Id'); ?></th>
		<th><?php echo __('District Id'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th><?php echo __('Activo'); ?></th>
		<th><?php echo __('Company Id'); ?></th>
		<th class="actions"></th>
	</tr>
	<thead>
	<tbody>
	<?php foreach ($company['WebUser'] as $webUser): ?>
		<tr>
			<td><?php echo $webUser['id']; ?></td>
			<td><?php echo $webUser['nombre']; ?></td>
			<td><?php echo $webUser['usuario']; ?></td>
			<td><?php echo $webUser['password']; ?></td>
			<td><?php echo $webUser['copiapassword']; ?></td>
			<td><?php echo $webUser['webuser_profile_id']; ?></td>
			<td><?php echo $webUser['district_id']; ?></td>
			<td><?php echo $webUser['created']; ?></td>
			<td><?php echo $webUser['modified']; ?></td>
			<td><?php echo $webUser['activo']; ?></td>
			<td><?php echo $webUser['company_id']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('<span class="glyphicon glyphicon-search"></span>'), array('controller' => 'web_users', 'action' => 'view', $webUser['id']), array('escape' => false)); ?>
				<?php echo $this->Html->link(__('<span class="glyphicon glyphicon-edit"></span>'), array('controller' => 'web_users', 'action' => 'edit', $webUser['id']), array('escape' => false)); ?>
				<?php echo $this->Form->postLink(__('<span class="glyphicon glyphicon-remove"></span>'), array('controller' => 'web_users', 'action' => 'delete', $webUser['id']), array('escape' => false), __('Are you sure you want to delete # %s?', $webUser['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</tbody>
	</table>
<?php endif; ?>

	<div class="actions">
		<?php echo $this->Html->link(__('<span class="glyphicon glyphicon-plus"></span>&nbsp;&nbsp;New Web User'), array('controller' => 'web_users', 'action' => 'add'), array('escape' => false, 'class' => 'btn btn-default')); ?> 
	</div>
	</div><!-- end col md 12 -->
</div>
