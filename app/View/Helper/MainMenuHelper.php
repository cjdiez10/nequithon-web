<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * CakePHP MainMenuHelper
 * @author CRISTIAN
 */
class MainMenuHelper extends AppHelper {

    
    var $helpers = array('Html'); 
    
    
    function getMainMenu2($links = array(), $imgsLinks = array(), $htmlAttributes = array(), $classUl, $type = 'ul'){       
        //$this->tags['ul'] = '<ul%s>%s</ul>';
        $this->tags['ul'] = '<ul '.$classUl.'%s>%s</ul>';
        
        $this->tags['ol'] = '<ol%s>%s</ol>'; 
        $this->tags['li'] = '<li%s>%s</li>';
        
        $out = array();      
        $i = 0;
        foreach ($links as $title => $link){
            /*
            if($this->url($link) == substr($this->here,0,-1)){
                
                $out[] = sprintf($this->tags['li'],' class="active"',$this->Html->link($title, $link)); 
            }else{
            */    
                $out[] = sprintf($this->tags['li'], ' class="active"', $this->Html->link( $title.'<span class="pull-right hidden-xs showopacity glyphicon '.$imgsLinks[$i].'"></span>', $link, array('escape' => false))); 
            //}
            $i++;
        } 
        $tmp = join("\n", $out); 
        return $this->output(sprintf($this->tags[$type],$this->_parseAttributes($htmlAttributes), $tmp)); 
    }
	
    /*
    function getMainMenu($links = array(), $htmlAttributes = array(), $classUl, $type = 'ul') 
    {       
        //$this->tags['ul'] = '<ul%s>%s</ul>';
        $this->tags['ul'] = '<ul '.$classUl.'%s>%s</ul>';
        
        $this->tags['ol'] = '<ol%s>%s</ol>'; 
        $this->tags['li'] = '<li%s>%s</li>';
        
        //$this->Html->link(__('<span class=\"glyphicon glyphicon-plus\"></span>&nbsp;&nbsp;New " . Inflector::humanize(Inflector::underscore($alias)) . "'), array('controller' => '{$details['controller']}', 'action' => 'add'), array('escape' => false));
        
        $out = array();         
        foreach ($links as $title => $link) 
        { 
            if($this->url($link) == substr($this->here,0,-1)) 
            { 
                $out[] = sprintf($this->tags['li'],' class="active"',$this->Html->link($title, $link)); 
            } 
            else 
            { 
                $out[] = sprintf($this->tags['li'],'',$this->Html->link($title, $link)); 
            } 
        } 
        $tmp = join("\n", $out); 
        return $this->output(sprintf($this->tags[$type],$this->_parseAttributes($htmlAttributes), $tmp)); 
    }
    */
    
    function getMainMenuWithSubmenus($nameMenu, $links = array(), $htmlAttributes = array(), $classUl, $type = 'ul') 
    {       
        $tmp = '<ul class="nav nav-pills nav-stacked">'
                . '<li class="dropdown">'
                . '<a href="#" class="dropdown-toggle" data-toggle="dropdown">'.$nameMenu.' <b class="caret"></b></a>'.
                $this->getMainMenu($links, $htmlAttributes, $classUl).''
                . '</li>'
                . '</ul>';
        
        return $tmp;
        //return $this->output(sprintf($this->tags[$type],$this->_parseAttributes($htmlAttributes), $tmp)); 
    }
    
    /*
    public $helpers = array();

    public function __construct(View $View, $settings = array()) {
        parent::__construct($View, $settings);
    }

    public function beforeRender($viewFile) {
        
    }

    public function afterRender($viewFile) {
        
    }

    public function beforeLayout($viewLayout) {
        
    }

    public function afterLayout($viewLayout) {
        
    }
    
    */

}
