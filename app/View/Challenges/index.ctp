<div class="challenges index">

    <div class="row">
        <div class="col-md-12">
            <div class="page-header">
                <br/>
                <?php echo $this->Session->flash(); ?>
                <div class="navbar-form navbar-right">
                    <center>
                        <table style="float: right;">
                        <tr>
                            <td align="center">
                                <?php
                        
                                echo $this->Html->link(  
                                    $this->Html->image('icons/new.png').'<br/>'.__('Crear un reto'), 
                                    DS.$this->params['controller'].DS."add",
                                    array('escape'=>false)
                                );
                                ?>
                                &nbsp;&nbsp;
                            </td>
                            <td>
                                <?php					
                                if( $this->Session->read("Auth.User")!=NULL && count($this->Session->read("Auth.User"))>0 ){

                                    echo $this->Html->link(__('Cerrar sesión'), array('controller' => 'web_users', 'action' => 'logout'), array('escape' => false, 'class' => 'btn btn-info'));
                                }
                                ?>
                            </td>
                        </tr>
                        </table>
                    </center>
                </div>
                <h1><?php echo __('Retos'); ?></h1>
            </div>
            
            <br/>
            <table cellpadding="0" cellspacing="0" class="table table-bordered">
                <thead>
                    <tr class="active">
                        <th><?php echo $this->Paginator->sort('nombre', 'Título'); ?></th>
                        <th><?php echo $this->Paginator->sort('fecha'); ?></th>
                        <th><?php echo $this->Paginator->sort('descripcion'); ?></th>
                        <!--<th><?php //echo $this->Paginator->sort('imagen'); ?></th>-->
                        <th><?php echo $this->Paginator->sort('company_id', 'Empresa'); ?></th>
                        <th><?php echo 'Estado'; ?></th>
                        <th class="actions"></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($challenges as $challenge): ?>
                        <tr>
                            <td><?php echo h($challenge['Challenge']['nombre']); ?>&nbsp;</td>
                            <td><?php echo date( 'M d, Y', strtotime($challenge['Challenge']['fecha']) ); ?>&nbsp;</td>
                            <td><?php echo h($challenge['Challenge']['descripcion']); ?>&nbsp;</td>
                            <!--<td><?php //echo h($challenge['Challenge']['imagen']); ?>&nbsp;</td>-->
                            <!--
                            <td>
                                <?php //echo $challenge['Company']['nombre'];//$this->Html->link($challenge['Company']['nombre'], array('controller' => 'companies', 'action' => 'view', $challenge['Company']['id'])); ?>
                            </td>
                            -->
                            <td align="center"><?php echo "<img src='". ($challenge['Company']['imagen'])."' width='100px;' />"; ?>&nbsp;</td>
                            
                            <?php 
                                $status = '';
                                $class = "";
                                    
                                if(strtotime( date('Y-m-d') ) <= strtotime( $challenge['Challenge']['fecha'] )){
                                    $status = ('Activo');
                                    $class = "classRowGreen";
                                }else{
                                    $status = ('Inactivo');
                                    $class = "classRowRed";
                                }
                            ?>
                            <!--<td class="fontWhite <?php //echo $class; ?>"><?php //echo $status; ?>&nbsp;</td>-->
                            <td><?php echo $status; ?>&nbsp;</td>
                            <td class="actions">
                                &nbsp;
                                <?php echo $this->Html->link('<span class="glyphicon glyphicon-search"></span>', array('action' => 'view', $challenge['Challenge']['id']), array('escape' => false)); ?>
                                &nbsp;&nbsp;
                                <?php echo $this->Html->link('<span class="glyphicon glyphicon-edit"></span>', array('action' => 'edit', $challenge['Challenge']['id']), array('escape' => false)); ?>
                                &nbsp;&nbsp;
                                <?php echo $this->Form->postLink('<span class="glyphicon glyphicon-remove"></span>', array('action' => 'delete', $challenge['Challenge']['id']), array('escape' => false), __('Seguro que deseas borrar el reto: %s?', $challenge['Challenge']['nombre'])); ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>

            <center>
            <br/>
            <p>
                <small><?php echo $this->Paginator->counter(array('format' => __('Página {:page} de {:pages}, mostrando el registro {:current} de {:count} en total, iniciando en {:start}, finalizando en {:end}'))); ?></small>
            </p>

            <?php
            $params = $this->Paginator->params();
            if ($params['pageCount'] > 1) {
                ?>
                <ul class="pagination pagination-sm">
                    <?php
                    echo $this->Paginator->prev('&larr; Anterior', array('class' => 'prev', 'tag' => 'li', 'escape' => false), '<a onclick="return false;">&larr; Anterior</a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
                    echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'currentClass' => 'active', 'currentTag' => 'a'));
                    echo $this->Paginator->next('Siguiente &rarr;', array('class' => 'next', 'tag' => 'li', 'escape' => false), '<a onclick="return false;">Siguiente &rarr;</a>', array('class' => 'next disabled', 'tag' => 'li', 'escape' => false));
                    ?>
                </ul>
            <?php } ?>
            </center>
            <br/>
            <br/>

        </div> <!-- end col md 9 -->
    </div><!-- end row -->


</div><!-- end containing of content -->