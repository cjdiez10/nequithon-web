<div class="challenges form">

    <div class="row">
        <div class="col-md-12">
            <div class="page-header">
                <br/>
                <?php echo $this->Session->flash(); ?>
                <div class="navbar-form navbar-right">
                    <center>
                        <table style="float: right;">
                        <tr>
                            <td align="center">
                                <?php
                                echo $this->Html->link(  
                                    __('Volver'), 
                                    //$this->request->referer(),
                                    DS.$this->params['controller'].DS,
                                    array('escape'=>false, 'class' => 'btn btn-success')
                                );
                                ?>
                                &nbsp;&nbsp;
                            </td>
                            <td>
                                <?php					
                                if( $this->Session->read("Auth.User")!=NULL && count($this->Session->read("Auth.User"))>0 ){

                                    echo $this->Html->link(__('Cerrar sesión'), array('controller' => 'web_users', 'action' => 'logout'), array('escape' => false, 'class' => 'btn btn-info'));
                                }
                                ?>
                            </td>
                        </tr>
                        </table>
                    </center>
                </div>
                <h1><?php echo __('Crear un reto'); ?></h1>
            </div>
            
            <br/>
            <?php echo $this->Form->create('Challenge', array('role' => 'form')); ?>
            
            <table width="100%" border="0" cellspacing="0" cellpadding="1">
                
                <tr align="center">
                    <td>
                        <?php echo $this->Form->input('nombre', array('class' => 'form-control', 'placeholder' => 'Título', 'label' => 'Como se llama el reto ?')); ?>
                    </td>
                    <td>
                        <?php echo $this->Form->input('fecha', array('type' => 'text', 'class' => 'form-control datepicker', 'placeholder' => 'Fecha', 'label' => 'Cuando será ?')); ?>
                    </td>
                </tr>
                
                <tr><td colspan="2">&nbsp;</td></tr>
                
                <!--
                <tr>
                    <td colspan="2">
                        <?php //echo $this->Form->input('imagen', array('class' => 'form-control', 'placeholder' => 'Imagen')); ?>
                    </td>
                </tr>
                
                <tr><td colspan="2">&nbsp;</td></tr>
                -->
                
                <tr>
                    <td>
                        <?php echo $this->Form->input('category_id', array('class' => 'form-control', 'label' => 'A que categoría pertenece ?',  'placeholder' => 'Categoria', 'empty' => 'Seleccione')); ?>
                    </td>
                    <td>
                        <?php echo $this->Form->input('company_id', array('class' => 'form-control', 'placeholder' => 'Empresa', 'label' => 'A que comercio va asociado ?')); ?>
                    </td>
                </tr>
                
                <tr><td colspan="2">&nbsp;</td></tr>
                
                <tr>
                    <td colspan="2"> 
                        <?php echo $this->Form->input('descripcion', array('class' => 'form-control', 'placeholder' => 'Describe aquí', 'rows' => 4, 'label' => 'Describe aquí el llamado a la acción de este reto')); ?>
                    </td>
                </tr>

                <tr><td colspan="2">&nbsp;</td></tr>
                
                <tr>
                    <td align="center" colspan="2">
                        <?php echo $this->Form->submit(__('Guardar'), array('class' => 'btn btn-success')); ?>
                    </td>
                </tr>
            </table>

            <?php echo $this->Form->end() ?>
            <br/><br/>

        </div><!-- end col md 12 -->
    </div><!-- end row -->
</div>


<?php 

//echo $this->Html->script('jquery/jquery-ui');
echo $this->Html->script('bootstrap-datepicker');
//echo $this->Html->script('fileinput.min');
echo $this->Html->css('datepicker'); 

echo $this->Html->script('bootstrap-timepicker.min');
echo $this->Html->css('bootstrap-timepicker.min'); 

echo $this->Html->script('bootbox.min');
echo $this->Html->script("scripts");
?>

<script type="text/javascript">var baseUrl = '<?php echo $this->webroot; ?>';</script>
<script language="javascript" type="text/javascript">
    
    
    $(document).ready(function() {
        
        $('.datepicker').datepicker({
            format: 'yyyy-mm-dd',
        });
     
        $('.datepicker').on('changeDate', function(ev){
            $(this).datepicker('hide');
        });
        
        //document.getElementById("btnSolicitarDomicilio").addEventListener("click", onSubmitForm);
        
    });
    
    
    function confirmSubmit(){
        
        bootbox.confirm("Seguro(a) que deseas enviar esta información ?", function(result){ 
            //console.log(result); return result; 
            //return result;
            if( result===true )
                document.ChallengeAddForm.submit();
        });
        return false;
    }
    
        
</script>