<div class="webUsers form">

    <div class="row">
        <div class="col-md-12">
            <div class="page-header">
                <br/>
                <div class="navbar-form navbar-right">
                    <center>
                        <table style="float: right;">
                        <tr>
                            <td align="center">
                                <?php
                        
                                echo $this->Html->link(  
                                    __('Volver'), 
                                    //$this->request->referer(),
                                    DS.$this->params['controller'].DS,
                                    array('escape'=>false, 'class' => 'btn btn-success')
                                );
                                ?>
                                
                                <?php //echo $this->Html->link(__('Volver'), $this->request->referer(), array('escape' => false)); ?>
                                
                                &nbsp;&nbsp;
                            </td>
                            <td>&nbsp;&nbsp;
                                <?php					
                                if( $this->Session->read("Auth.User")!=NULL && count($this->Session->read("Auth.User"))>0 ){

                                    echo $this->Html->link(__('Cerrar sesión'), array('controller' => 'web_users', 'action' => 'logout'), array('escape' => false, 'class' => 'btn btn-info'));
                                }
                                ?>
                            </td>
                        </tr>
                        </table>
                    </center>                    
                </div>
                <?php echo $this->Session->flash(); ?>
                <h1><?php echo __('Crear un usuario'); ?></h1>
            </div>
            <br/>
            <?php echo $this->Form->create('WebUser', array('role' => 'form')); ?>
            
            <table width="100%">
                <tr>
                    <td>
                        <?php echo $this->Form->input('webuser_profile_id', array('class' => 'form-control', 'placeholder' => 'Webuser Profile Id', 'label' => 'Perfil de Usuario')); ?>
                    </td>
                    <td>
                        <?php echo $this->Form->input('usuario', array('class' => 'form-control', 'placeholder' => 'Usuario')); ?>
                    </td>
                </tr>
                <tr><td colspan="2">&nbsp;</td></tr>
                <tr>
                    <td>
                        <?php echo $this->Form->input('password', array('class' => 'form-control', 'placeholder' => 'Password')); ?>
                    </td>
                    <td>
                        <?php echo $this->Form->input('copiapassword', array('class' => 'form-control', 'placeholder' => 'Copia password', 'label' => 'Copia del Password')); ?>
                    </td>
                </tr>
                <tr><td colspan="2">&nbsp;</td></tr>
                <tr>
                    <td>
                        <?php //echo $this->Form->input('district_id', array('class' => 'form-control', 'placeholder' => 'Distrito', 'label' => 'Distrito')); ?>
                    </td>
                    <td>
                        <?php echo $this->Form->input('activo', array('class' => 'form-control', 'placeholder' => 'Activo', 'type' => 'select', 'options' => $activeOptions)); ?>
                    </td>
                </tr>
                <tr><td colspan="2">&nbsp;</td></tr>
                <tr>
                    <td colspan="2">
                        <?php //echo $this->Form->input('company_id', array('class' => 'form-control', 'placeholder' => 'Empresa', 'label' => 'Empresa')); ?>
                    </td>
                </tr>
            </table>
            <br/><br/>
            <center>
                <div class="form-group">
                    <?php echo $this->Form->submit(__('Guardar'), array('class' => 'btn btn-success')); ?>
                </div>
            </center>

            <?php echo $this->Form->end() ?>

        </div><!-- end col md 12 -->
    </div><!-- end row -->
</div>
