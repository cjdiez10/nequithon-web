<div class="webUsers index">

    <div class="row">
        <div class="col-md-12">
            <div class="page-header">
                <br/>
                <div class="navbar-form navbar-right">
                    <center>
                        <table style="float: right;">
                        <tr>
                            <td align="center">
                                <?php
                        
                                echo $this->Html->link(  
                                    $this->Html->image('icons/new.png').'<br/>'.__('Crear Usuario'), 
                                    "/web_users/add",
                                    array('escape'=>false)
                                );
                                ?>
                                &nbsp;&nbsp;
                            </td>
                            <td>&nbsp;&nbsp;
                                <?php					
                                if( $this->Session->read("Auth.User")!=NULL && count($this->Session->read("Auth.User"))>0 ){

                                    echo $this->Html->link(__('Cerrar sesión'), array('controller' => 'web_users', 'action' => 'logout'), array('escape' => false, 'class' => 'btn btn-info'));
                                }
                                ?>
                            </td>
                        </tr>
                        </table>
                    </center>                    
                </div>
                <?php echo $this->Session->flash(); ?>
                <h1><?php echo __('Usuarios web'); ?></h1>
            </div>
            
            <br/>
            <table cellpadding="0" cellspacing="0" class="table table-bordered">
                <thead>
                    <tr class="active">
                        <th><?php echo $this->Paginator->sort('usuario'); ?></th>
                        <th><?php echo h('Contraseña'); //$this->Paginator->sort('password'); ?></th>
                        <th><?php echo $this->Paginator->sort('webuser_profile_id', 'Perfil de Usuario'); ?></th>
                        <!--<th><?php //echo $this->Paginator->sort('company_id', 'Empresa'); ?></th>-->
                        <!--<th><?php //echo $this->Paginator->sort('modified'); ?></th>-->
                        <th><?php echo $this->Paginator->sort('activo'); ?></th>
                        <th class="actions"></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($webUsers as $webUser): ?>
                    <tr align="center">
                            <td><?php echo h($webUser['WebUser']['usuario']); ?></td>
                            <td><a href="#" onclick="bootbox.alert('<?php echo h($webUser['WebUser']['copiapassword']); ?>')">Click aquí</a></td>
                            <td>
                                <?php echo h($webUser['WebuserProfile']['nombre']);//$this->Html->link($webUser['WebuserProfile']['nombre'], array('controller' => 'webuser_profiles', 'action' => 'view', $webUser['WebuserProfile']['id'])); ?>
                            </td>
                            <!--<td><?php //echo h($webUser['Company']['nombre']); ?>&nbsp;</td>-->
                            <!--<td><?php //echo h($webUser['WebUser']['modified']); ?>&nbsp;</td>-->
                            <td><?php if( $webUser['WebUser']['activo']=='S' ) echo 'Si'; else echo 'No';//echo h($webUser['WebUser']['activo']); ?></td>
                            <td>
                                <?php echo $this->Html->link('<span class="glyphicon glyphicon-search"></span>', array('action' => 'view', $webUser['WebUser']['id']), array('escape' => false)); ?>
                                &nbsp;&nbsp;
                                <?php echo $this->Html->link('<span class="glyphicon glyphicon-edit"></span>', array('action' => 'edit', $webUser['WebUser']['id']), array('escape' => false)); ?>
                                <?php //echo $this->Form->postLink('<span class="glyphicon glyphicon-remove"></span>', array('action' => 'delete', $webUser['WebUser']['id']), array('escape' => false), __('Are you sure you want to delete # %s?', $webUser['WebUser']['id'])); ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>

            <br/>
            <center>
            <p>
                <small><?php echo $this->Paginator->counter(array('format' => __('Página {:page} de {:pages}, mostrando {:current} registros de {:count} en total, iniciando en el registro {:start}, finalizando en {:end}'))); ?></small>
            </p>

            <?php
            $params = $this->Paginator->params();
            if ($params['pageCount'] > 1) {
                ?>
                <ul class="pagination pagination-sm">
                    <?php
                    echo $this->Paginator->prev('&larr; Anterior', array('class' => 'prev', 'tag' => 'li', 'escape' => false), '<a onclick="return false;">&larr; Anterior</a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
                    echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'currentClass' => 'active', 'currentTag' => 'a'));
                    echo $this->Paginator->next('Siguiente &rarr;', array('class' => 'next', 'tag' => 'li', 'escape' => false), '<a onclick="return false;">Siguiente &rarr;</a>', array('class' => 'next disabled', 'tag' => 'li', 'escape' => false));
                    ?>
                </ul>
            <?php } ?>
            </center>

        </div> <!-- end col md 9 -->
    </div><!-- end row -->


</div><!-- end containing of content -->

<?php 
echo $this->Html->script('bootbox.min');
?>