<div class="webUsers view">
    
    <div class="row">
        <div class="col-md-12">
            <div class="page-header">
                <br/>
                <div class="navbar-form navbar-right">
                    <center>
                        <table style="float: right;">
                        <tr>
                            <td align="center">
                                <?php
                        
                                echo $this->Html->link(  
                                    __('Volver'), 
                                    //$this->request->referer(),
                                    DS.$this->params['controller'].DS,
                                    array('escape'=>false, 'class' => 'btn btn-success')
                                );
                                ?>
                                &nbsp;&nbsp;
                            </td>
                            <td>&nbsp;&nbsp;
                                <?php					
                                if( $this->Session->read("Auth.User")!=NULL && count($this->Session->read("Auth.User"))>0 ){

                                    echo $this->Html->link(__('Cerrar sesión'), array('controller' => 'web_users', 'action' => 'logout'), array('escape' => false, 'class' => 'btn btn-info'));
                                }
                                ?>
                            </td>
                        </tr>
                        </table>
                    </center>                    
                </div>
                <?php echo $this->Session->flash(); ?>
                <h1><?php echo __('Usuario'); ?></h1>
            </div><!--
        </div>
    </div>

    <div class="row">
        <div class="col-md-3">
            <div class="actions">
                <div class="panel panel-default">
                    <div class="panel-heading">Menú</div>
                    <div class="panel-body">
                        <ul class="nav nav-pills nav-stacked">
                            <li><?php //echo $this->Html->link(__('Volver'), $this->request->referer(), array('escape' => false)); ?> </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-9">
            -->
            <br/>
            <table cellpadding="0" cellspacing="0" class="table table-bordered">
                <tbody align="center">
                    <tr>
                        <th class="active"><?php echo __('Usuario'); ?></th>
                        <td>
                            <?php echo h($webUser['WebUser']['usuario']); ?>
                            &nbsp;
                        </td>
                    </tr>
                    <!--
                    <tr>
                        <th class="active"><?php //echo __('Contraseña'); ?></th>
                        <td>
                            <?php //echo h($webUser['WebUser']['password']); ?>
                            &nbsp;
                        </td>
                    </tr>
                    -->
                    <tr>
                        <th class="active"><?php echo __('Contraseña'); ?></th>
                        <td>
                            <?php echo h($webUser['WebUser']['copiapassword']); ?>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <th class="active"><?php echo __('Perfil de usuario'); ?></th>
                        <td>
                            <?php echo $this->Html->link($webUser['WebuserProfile']['nombre'], array('controller' => 'webuser_profiles', 'action' => 'view', $webUser['WebuserProfile']['id'])); ?>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <th class="active"><?php echo __('Distrito'); ?></th>
                        <td>
                            <?php echo $this->Html->link($webUser['District']['nombre'], array('controller' => 'districts', 'action' => 'view', $webUser['District']['id'])); ?>
                            &nbsp;
                        </td>
                    </tr>
                    <!--
                    <tr>
                        <th class="active"><?php //echo __('Created'); ?></th>
                        <td>
                            <?php //echo h($webUser['WebUser']['created']); ?>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <th class="active"><?php //echo __('Modified'); ?></th>
                        <td>
                            <?php //echo h($webUser['WebUser']['modified']); ?>
                            &nbsp;
                        </td>
                    </tr>
                    -->
                    <tr>
                        <th class="active"><?php echo __('Activo'); ?></th>
                        <td>
                            <?php if( $webUser['WebUser']['activo']=='S') echo 'Si'; else echo 'No'; ?>
                            &nbsp;
                        </td>
                    </tr>
                </tbody>
            </table>

        </div><!-- end col md 9 -->

    </div>
</div>

