<div class="webUsers form">
    
    <?php echo $this->Session->flash('auth'); ?>
    <div class="row">
        <div class="col-md-12">
            <div class="page-header">
                <?php echo $this->Session->flash(); ?>
                <br/><br/>
                <h1><?php echo __('Por favor ingrese su usuario y contraseña'); ?></h1>
            </div>
        </div>
    </div>

    <br/>
    <div class="row">                
        <div class="col-md-3">
            <div class="actions">
                <div class="panel panel-default">
                    <div class="panel-heading">Login</div>
                    <div class="panel-body">
                        <ul class="nav nav-pills nav-stacked">
                            <!--
                            <li><?php //echo $this->Html->link(__('<span class="glyphicon glyphicon-list"></span>&nbsp;&nbsp;List Web Users'), array('action' => 'index'), array('escape' => false)); ?></li>
                            <li><?php //echo $this->Html->link(__('<span class="glyphicon glyphicon-list"></span>&nbsp;&nbsp;List Webuser Profiles'), array('controller' => 'webuser_profiles', 'action' => 'index'), array('escape' => false)); ?> </li>
                            <li><?php //echo $this->Html->link(__('<span class="glyphicon glyphicon-plus"></span>&nbsp;&nbsp;New Webuser Profile'), array('controller' => 'webuser_profiles', 'action' => 'add'), array('escape' => false)); ?> </li>
                            -->
                            <center>
                            <?php //echo $this->Html->image('logo_f.png', array('alt' => 'Demo Falabella', 'border' => '0') ); ?>
                                <?php echo $this->Html->image('logo.png', array('alt' => 'Logo LogiMetrica', 'border' => '0', 'width' => '150px', 'height' => '150px') ); ?>
                            </center>
                        </ul>
                    </div>
                </div>
            </div>			
        </div><!-- end col md 3 -->        
        <div class="col-md-9">
            
            <?php echo $this->Form->create('WebUser'/*, array('role' => 'form')*/); ?>

                <div class="form-group">
                    <?php echo $this->Form->input('usuario', array('class' => 'form-control', 'placeholder' => 'Usuario'));?>
                </div>
                <br/>
                <div class="form-group">
                    <?php echo $this->Form->input('password', array('class' => 'form-control', 'placeholder' => '********', 'label' => 'Contraseña'));?>
                </div>
                <br/>
                <center>
                <div class="form-group">
                    <?php echo $this->Form->submit(__('Login'), array('class' => 'btn btn-success')); ?>
                </div>
                </center>
            
            <?php //echo $this->Form->end('Login') ?>
        </div><!-- end col md 12 -->        
    </div><!-- end row -->
</div>