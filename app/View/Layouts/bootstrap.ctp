<!DOCTYPE html>
<html lang="en">
  <head>
    <title><?php echo $title_for_layout; ?></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <?php
        echo $this->Html->meta('icon');
        echo $this->fetch('meta');
        echo $this->fetch('css');
        echo $this->fetch('script');        
    ?>

    <?php echo $this->Html->css("bootstrap.min"); ?>

    <!-- Latest compiled and minified JavaScript -->
    <?php echo $this->Html->script("jquery/jquery.min"); ?>
    <?php echo $this->Html->script("bootstrap.min"); ?>
    
    <style type="text/css">
    	body{ /*padding: 70px 0px;*/ }
    </style>

    <script type="text/javascript">var baseUrl = '<?php echo $this->webroot;//$this->base; ?>';</script>
    
  </head>

  <?php echo $this->Js->writeBuffer(); // Write cached scripts ?>
  <body>

    <?php echo $this->Element('navigation'); ?>

    <div class="container">
    
        <?php //echo Configure::version(); ?>        
        <?php //echo $this->Session->flash(); ?>

        <?php echo $this->fetch('content'); ?>

    </div><!-- /.container -->
    
    <?php //echo $this->element('sql_dump'); ?>

  </body>
</html>
