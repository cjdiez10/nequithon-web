<div class="data index">

    <div class="row">
        <div class="col-md-12">
            <div class="page-header">
                <br/>
                <?php echo $this->Session->flash(); ?>
                <div class="navbar-form navbar-right">
                    <center>
                        <table style="float: right;">
                        <tr>
                            <td align="center">
                                <?php
                        
                                echo $this->Html->link(  
                                    $this->Html->image('icons/new.png').'<br/>'.__('Importar datos'), 
                                    DS.$this->params['controller'].DS."import",
                                    array('escape'=>false)
                                );
                                ?>
                                &nbsp;&nbsp;
                            </td>
                            <td>
                                <?php					
                                if( $this->Session->read("Auth.User")!=NULL && count($this->Session->read("Auth.User"))>0 ){

                                    echo $this->Html->link(__('Cerrar sesión'), array('controller' => 'web_users', 'action' => 'logout'), array('escape' => false, 'class' => 'btn btn-info'));
                                }
                                ?>
                            </td>
                        </tr>
                        </table>
                    </center>
                </div>
                <h1><?php echo __('Data Transaccional'); ?></h1>
            </div>
            
            <br/>
            <table cellpadding="0" cellspacing="0" class="table table-bordered">
                <thead>
                    <tr class="active">
                        <th><?php echo 'Celular usuario Nequi';//$this->Paginator->sort('telefono'); ?></th>
                        <th><?php echo 'Info transacción'//$this->Paginator->sort('descripcion'); ?></th>
                        <th><?php echo $this->Paginator->sort('fecha'); ?></th>
                        <th><?php echo 'Valor';//$this->Paginator->sort('valor'); ?></th>
                        <!--<th><?php //echo $this->Paginator->sort('created'); ?></th>-->
                        <th class="actions"></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($data as $data): ?>
                        <tr>
                            <td><?php echo h($data['Data']['telefono']); ?>&nbsp;</td>
                            <td><?php echo h($data['Data']['descripcion']); ?>&nbsp;</td>
                            <td><?php echo date( 'M d, Y', strtotime($data['Data']['fecha'])); ?>&nbsp;</td>
                            <td><?php echo '$'.number_format($data['Data']['valor'], 0); ?>&nbsp;</td>
                            <!--<td><?php //echo h($data['Data']['created']); ?>&nbsp;</td>-->
                            <td class="actions">
                                &nbsp;
                                <?php echo $this->Html->link('<span class="glyphicon glyphicon-search"></span>', array('action' => 'view', $data['Data']['id']), array('escape' => false)); ?>
                                &nbsp;&nbsp;
                                <?php echo $this->Html->link('<span class="glyphicon glyphicon-edit"></span>', array('action' => 'edit', $data['Data']['id']), array('escape' => false)); ?>
                                &nbsp;&nbsp;
                                <?php echo $this->Form->postLink('<span class="glyphicon glyphicon-remove"></span>', array('action' => 'delete', $data['Data']['id']), array('escape' => false), __('Are you sure you want to delete # %s?', $data['Data']['id'])); ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>

            <br/>
            <center>
            <p>
                <small><?php echo $this->Paginator->counter(array('format' => __('Página {:page} de {:pages}, mostrando el registro {:current} de {:count} en total, iniciando en {:start}, finalizando en {:end}'))); ?></small>
            </p>

            <?php
            $params = $this->Paginator->params();
            if ($params['pageCount'] > 1) {
                ?>
                <ul class="pagination pagination-sm">
                    <?php
                    echo $this->Paginator->prev('&larr; Anterior', array('class' => 'prev', 'tag' => 'li', 'escape' => false), '<a onclick="return false;">&larr; Anterior</a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
                    echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'currentClass' => 'active', 'currentTag' => 'a'));
                    echo $this->Paginator->next('Siguiente &rarr;', array('class' => 'next', 'tag' => 'li', 'escape' => false), '<a onclick="return false;">Siguiente &rarr;</a>', array('class' => 'next disabled', 'tag' => 'li', 'escape' => false));
                    ?>
                </ul>
            <?php } ?>
            </center>

        </div> <!-- end col md 9 -->
    </div><!-- end row -->


</div><!-- end containing of content -->