<div class="data form">

    <div class="row">
        <div class="col-md-12">            
            <div class="page-header">
                <br/>
                <?php echo $this->Session->flash(); ?>
                <div class="navbar-form navbar-right">
                    <center>
                        <table style="float: right;">
                            <tr>
                                <td align="center">
                                    <?php

                                    echo $this->Html->link(  
                                        __('Volver'), 
                                        //$this->request->referer(),
                                        DS.$this->params['controller'].DS,
                                        array('escape'=>false, 'class' => 'btn btn-success')
                                    );
                                    ?>
                                    &nbsp;&nbsp;
                                </td>
                                <!--
                                <td align="center">
                                <?php
                                /*
                                  echo $this->Html->link(
                                  $this->Html->image('icons/new.png').'<br/>'.__('Crear Punto'),
                                  "/points/add",
                                  array('escape'=>false)
                                  );
                                 */
                                ?>
                                    &nbsp;&nbsp;
                                </td>
                                -->
                                <td><!--&nbsp;&nbsp;-->
                                    <?php
                                    if ($this->Session->read("Auth.User") != NULL && count($this->Session->read("Auth.User")) > 0) {

                                        //usar solo este
                                        //echo $this->Html->link(	$this->Html->image('icons/exit.png'), '/web_users/logout', array('escape' => false) );
                                        //echo $this->Html->link( 'Cerrar sesion', '/web_users/logout' );

                                        echo $this->Html->link(__('Cerrar sesión'), array('controller' => 'web_users', 'action' => 'logout'), array('escape' => false, 'class' => 'btn btn-info'));
                                    }
                                    ?>
                                </td>
                            </tr>
                        </table>
                    </center>
                </div>
                <h1><?php echo __('Importar Datos'); ?></h1>
            </div>
            
            <br/>
            <?php echo $this->Form->create('Data', array('role' => 'form', 'onSubmit' => 'return confirmSubmit();', 'name' => 'DataImportForm', 'enctype' => 'multipart/form-data')); ?>

            <!-- classRowGreen classTxtGreen-->
            <table width="100%" border="0" cellspacing="0" cellpadding="1">
                
                <tr><td colspan="2">&nbsp;</td></tr>

                <tr align="center">
                    <!--
                    <td>
                        <?php 
                        //echo $this->Form->input('company_id', array('class' => 'form-control', 'placeholder' => 'Empresa', 'label' => 'Empresa', /*$empty,*/ 'empty' => 'Seleccione una', 'default' => $company_id, 'required' ));
                        ?>
                    </td>
                    -->
                    <td>
                        <?php 
                        echo $this->Form->input('excelfile', array('type' => 'file', 'class' => 'form-control', 'placeholder' => 'Archivo excel', 'label' => 'Seleccione un archivo', 'accept' => '.xls,.xlsx', 'required' ));
                        ?>
                    </td>
                </tr>
                <tr><td colspan="2">&nbsp;</td></tr>

            </table>

            <br/><br/>
            <center>
                <div class="form-group">
                    <?php echo $this->Form->submit(__('Cargar datos'), array('class' => 'btn btn-success'/* , 'onclick' => 'bootbox.confirm("Seguro(a) que deseas enviar esta información ?", function(result){ console.log(result); return result; });' */)); ?>
                </div>
            </center>

            <?php echo $this->Form->end() ?>
            <br/><br/><br/>

        </div><!-- end col md 12 -->
    </div><!-- end row -->
</div>

<?php
echo "<script type='text/javascript'>";

echo "</script>";

echo $this->Html->script('bootbox.min');
echo $this->Html->script("scripts");
?>


<script type="text/javascript">var baseUrl = '<?php echo $this->webroot; ?>';</script>
<script language="javascript" type="text/javascript">


    $(document).ready(function() {

    });


    function confirmSubmit() {

        bootbox.confirm("Seguro(a) que desea enviar esta información ?", function(result) {
            //console.log(result); return result; 
            //return result;
            if (result === true)
                document.DataImportForm.submit();
        });
        return false;
    }


</script>                