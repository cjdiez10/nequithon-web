<?php echo $this->Html->css('left_menu'); ?>

<div class="navbar navbar-inverse navbar-fixed-top">
  <div class="container">
    <div class="collapse navbar-collapse">
              
        <center>
            <table width="100%" style=" color: #ffffff">
                <tr>
                    <td width="25%">
                        <?php if( $this->Session->read("Auth.User")!=NULL && count($this->Session->read("Auth.User"))>0 ){ 
                            echo ('<h6><b>Usuario:</b>&nbsp;' . AuthComponent::user('nombre').'</h6>');
                        } ?>                        
                    </td>
                    <td width="60%" align="center">
                        <h6><b>NEQUITHON </b> - CIUDAD ORIGEN</h6>
                    </td>
                    <td width="15%">
                    </td>
                </tr>
            </table>
        </center>
    </div><!--/.nav-collapse -->
  </div>
</div>

<?php 
if( ($this->Session->read("Auth.User") != NULL && count($this->Session->read("Auth.User")) > 0) /*&&
        !($this->params['controller']=='bills' && $this->params['action']=='index') &&
        !($this->params['controller']=='drivers' && $this->params['action']=='tracking')*/ ){
?>
            
<nav style=" margin-top: 40px;" class="navbar navbar-default sidebar" role="navigation">
    <div class="container-fluid">
    <div class="collapse navbar-collapse" id="bs-sidebar-navbar-collapse-1">
        
        <?php 
        
            echo AppController::getSubMenu2($this->params['controller'], $this->params['action']); 
        ?>
        
    </div>
  </div>
</nav>
<?php } ?>
