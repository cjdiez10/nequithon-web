
function validarNumeros(e, patron) { 
    tecla = (document.all) ? e.keyCode : e.which; 
    if (tecla==8) 
        return true; //Tecla de retroceso (para poder borrar) 
    // dejar la línea de patron que se necesite y borrar el resto    
    te = String.fromCharCode(tecla); 
    return patron.test(te);
}

function numberFormat(inputid){
    
    var valor = document.getElementById(inputid).value;
    if( valor!==null && valor!=='' ){
        document.getElementById(inputid).value =
            parseFloat(valor.replace(/,/g, ""))
            .toFixed(0).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }
}