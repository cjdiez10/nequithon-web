<?php
//App::uses('AppController', 'Controller');
App::uses('Folder', 'Utility');
App::uses('File', 'Utility');

/**
 * DB sincronizer shell
 *
 */
class CronJobsShell extends AppShell {

    var $helpers = array('Html');

    
    
    public function botEnviarReto(){
        
        $this->loadModel('Challenge');
        $this->Challenge->recursive = -1;
        $this->Challenge->Behaviors->load('Containable');
        $this->Challenge->contain(
            array( 'Category' => array('fields' => array('Category.nombre')))
        );
        //$activeChallenges = $this->Challenge->find('all', array('conditions' => array("fecha <= '".date('Y-m-d')."'" )));
        $challenges = $this->Challenge->find('all', array('conditions' => array("fecha" => date('Y-m-d')) ));
        
        
        $this->loadModel('Data');
        $this->Data->recursive = -1;
        $bigData = $this->Data->find('all'/*, array('conditions' => array("fecha <= '".date('Y-m-d')."'" ))*/);        
        
        
        if( $bigData!=NULL && count($bigData)>0 ){
            
            foreach ($challenges as $challenge){
                
                foreach ($bigData as $data){
                    
                    //$keyWordsBigData = explode(" ", strtolower($data['Data']['descripcion']));
                    
                    if( strpos(strtolower($data['Data']['descripcion']), strtolower($challenge['Challenge']['Category']['nombre']))!=false ){
                       
                        //enviar el reto
                        //$challenge['Challenge']['telefono']
                        
                        
                    }
                }
            }
        }
        
        $this->setContentFile( 'CronJobsShell_botEnviarReto', "\nFecha: ".date('Y-m-d H:i:s')."...\n", true );
    }
    
    
    
    private function setContentFile($filename, $content, $add){
        
        $fichero = WWW_ROOT.$filename.'.txt';
        if( $add==true ){
            $actual = file_get_contents($fichero);
            $content = ($actual.$content);
        }
        file_put_contents($fichero, $content);
    }
    
    
    /**
     * Enviar un "heartbeat" a los que no hayan mandado gps en los ultimos 6 minutos
     * para reactivar el GpsTrackingService en la app
     */
    public function sendHeartbeat(){
        
        $message = '';
        
        $this->loadModel('GpsTracking');        
        $this->GpsTracking->recursive = -1;
        
        $infoTracking = $this->GpsTracking->find( 'all', 
            array(
                'fields' => array( 'GpsTracking.id', 'GpsTracking.driver_id', 'GpsTracking.fecha' ),
                'conditions' => array(
                    "GpsTracking.fecha" => date('Y-m-d'),
                    "TIME_TO_SEC( TIMEDIFF( GpsTracking.ultimahora, CURTIME() ) ) <= -360",
                ),
                //'fields' => array('GpsTracking.coordenadas', 'GpsTracking.modified', 'GpsTracking.ultimacoordenada', 'GpsTracking.ultimahora', 'GpsTracking.driver_id')
            )
        );
        
        if( $infoTracking!=NULL && count($infoTracking)>0 ){
            
            $tokensDrivers = array();
            $i = 0;
            foreach ($infoTracking as $driverGps) {
                
                $this->loadModel('Driver');        
                $this->Driver->recursive = -1;
                
                $driver = $this->Driver->findById( $driverGps['GpsTracking']['driver_id'] );
                
                if( $driver!=NULL && count($driver)>0 ){
                    
                    if( $driver['Driver']['tokenpush']!=NULL ){
                        $tokensDrivers[$i] = $driver['Driver']['tokenpush'];
                        $i++;
                    }
                }
            }
            
            if( $tokensDrivers!=NULL && count($tokensDrivers)>0 ){
                
                /*
                $tokensDrivers = implode( "' , '", $tokensDrivers);
                $conditionTruequesUsuario = array("Driver.publicadopor IN ('" . $tokensDrivers . "')");
                */
                
                /*
                require_once(APP . 'Lib' . DS . 'Utils.php');
                $json = httpConnectionToJson(  
                    "https://fcm.googleapis.com/fcm/send",
                    "POST",
                    array( 'Content-Type: application/json', 'Authorization: key=AAAA3OLHiFA:APA91bHUZkWxWPYwDLhDd-Mj3CnnnQrB6Oz05z_p5XaNWrTmCppFvFc5FvNwFgwCeGMTsOT-tWTlu1kGKxKbmb3gmV5y1pkF76arVOFoXsNcGKYiHLsFNHbzQOc-fXQL4JNZj39dZGNx'),
                    '{
                        "to": "/topics/lmcamiones",
                        "data": {
                               "accion": "heartbeat"
                            }
                    }'
                );
                */
                
                
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_URL, "https://fcm.googleapis.com/fcm/send");
                
                curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    'Content-Type: application/json',
                    'Authorization: key=AAAA3OLHiFA:APA91bHUZkWxWPYwDLhDd-Mj3CnnnQrB6Oz05z_p5XaNWrTmCppFvFc5FvNwFgwCeGMTsOT-tWTlu1kGKxKbmb3gmV5y1pkF76arVOFoXsNcGKYiHLsFNHbzQOc-fXQL4JNZj39dZGNx'
                ));
                
                $params = '{
                    "to": "/topics/lmcamiones",
                    "data": {
                           "accion": "heartbeat"
                        }
                }';
                
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt( $ch, CURLOPT_POSTFIELDS, $params );
                
                $result = curl_exec($ch);
                curl_close($ch);
                
                $json = json_decode($result, true);
                
                
                $this->setContentFile( 'SincronizerDbShell_sendHeartbeat', "\nSending heartbeat: ".$json/*json_encode($result)*/." Fecha:".date('Y-m-d H:i:s')."...\n", true );
            }
        }
        
        //$response = array( 'success' => true, 'message' => $message, 'data' => NULL );
        //$this->set( array( 'response' => $response, '_serialize' => array('response') ));
    }
    
    public function pruebaSendMail(){
        
        $message = '';
        
        try {
            App::uses('CakeEmail', 'Network/Email');
            
            $Email = new CakeEmail('bluehost_logimetrica');
            $Email->to('cjdiez@hotmail.com');
            $Email->subject('LogiMetrica - Prueba email');
            $Email->from(array('contacto@logimetrica.com' => 'LogiMetrica'));
            $Email->emailFormat('html');
            $Email->template('email_welcome', 'plantilla');
            $Email->viewVars(
                array(
                    'asunto' => 'LOGIMETRICA - PRUEBA SEND MAIL',
                    'info' => 'Enviando email en la fecha: '.date('Y-m-d H:i:s'),
                    'nombre' => '',//$barber['Barber']['nombre'],
                    'email' => '',//$barber['Barber']['email'],
                    'password' => '',//$barber['Barber']['password'],
                    'url_ws_confirmar' => '',
                    'more_info' => ''
                )
            );

            $message =  $Email->send();

        } catch (Exception $e) {}
        
                
        $fichero = WWW_ROOT.'SincronizerDbShell_pruebasendMail.txt';
        $actual = file_get_contents($fichero);
        //file_put_contents($fichero, ($actual."\nSincronizando distrito:".$distrito.", bodega: ".$bodega."...\n".$url." ".$message));
        file_put_contents($fichero, ($actual."\nSending email: ".$message." Fecha:".date('Y-m-d H:i:s')."...\n"));
        
        //$response = array( 'success' => true, 'message' => $message, 'data' => NULL );
        //$this->set( array( 'response' => $response, '_serialize' => array('response') ));
    }    
   
}