<?php

App::uses('AppController', 'Controller');

/**
 * WebUsers Controller
 *
 * @property WebUser $WebUser
 * @property PaginatorComponent $Paginator
 */
class WebUsersController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator');

    /**
     * index method
     *
     * @return void
     */
    public function index() {
        
        $this->paginate = array(
                //'fields' => $fields,
                'conditions' => array('WebUser.id != ?' => 1)//usuario cjdiez@hotmail.com
                //$conditions
            );
        
        $this->WebUser->recursive = 0;
        $result = $this->paginate();
        
        $this->set('webUsers', $result);
    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null) {
        if (!$this->WebUser->exists($id)) {
            throw new NotFoundException(__('Usuario invalido'));
        }
        $options = array('conditions' => array('WebUser.' . $this->WebUser->primaryKey => $id));
        $this->set('webUser', $this->WebUser->find('first', $options));
    }

    /**
     * add method
     *
     * @return void
     */
    public function add() {
        if ($this->request->is('post')) {
            $this->WebUser->create();
            
            $this->request->data['WebUser'] = array_map('trim', $this->request->data['WebUser']);
            
            if ($this->WebUser->save($this->request->data)) {
                $this->Session->setFlash(__('Usuario guardado exitosamente.'), 'default', array('class' => 'alert alert-success'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('El usuario no pudo ser guardado. Por favor, intente de nuevo.'), 'default', array('class' => 'alert alert-danger'));
            }
        }
        //$webuserProfiles = $this->WebUser->WebuserProfile->find('list', array('fields' => array('id', 'nombre') ));
        //$districts = $this->WebUser->District->find('list', array('fields' => array('id', 'nombre')));
        $activeOptions = array('S' => 'Si', 'N' => 'No');
        //$companies = $this->WebUser->Company->find('list', array('fields' => array('id', 'nombre')));
        $this->set(compact('webuserProfiles'/*, 'districts', 'activeOptions', 'companies'*/));
    }

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null) {
        if (!$this->WebUser->exists($id)) {
            throw new NotFoundException(__('Usuario invalido'));
        }
        if ($this->request->is(array('post', 'put'))) {
            
            $this->request->data['WebUser'] = array_map('trim', $this->request->data['WebUser']);
            
            if ($this->WebUser->save($this->request->data)) {
                $this->Session->setFlash(__('Usuario guardado exitosamente.'), 'default', array('class' => 'alert alert-success'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('El usuario no pudo ser guardado. Por favor, intente de nuevo.'), 'default', array('class' => 'alert alert-danger'));
            }
        } else {
            $options = array('conditions' => array('WebUser.' . $this->WebUser->primaryKey => $id));
            $this->request->data = $this->WebUser->find('first', $options);
        }
        /*
        $webuserProfiles = $this->WebUser->WebuserProfile->find('list', array('fields' => array('id', 'nombre') ));
        $districts = $this->WebUser->District->find('list', array('fields' => array('id', 'nombre')));
        $activeOptions = array('S' => 'Si', 'N' => 'No');
        $companies = $this->WebUser->Company->find('list', array('fields' => array('id', 'nombre')));
        */
        $this->set(compact('webuserProfiles'/*, 'districts', 'activeOptions', 'companies'*/));
    }

    /**
     * delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function delete($id = null) {
        $this->WebUser->id = $id;
        if (!$this->WebUser->exists()) {
            throw new NotFoundException(__('Usuario invalido'));
        }
        $this->request->allowMethod('post', 'delete');
        if ($this->WebUser->delete()) {
            $this->Session->setFlash(__('Usuario borrado exitosamente.'), 'default', array('class' => 'alert alert-success'));
        } else {
            $this->Session->setFlash(__('El usuario no pudo ser borrado. Por favor, intente de nuevo.'), 'default', array('class' => 'alert alert-danger'));
        }
        return $this->redirect(array('action' => 'index'));
    }
    
    
    public function login() {

        // if we get the post information, try to authenticate
        if ($this->request->is('post')) {

            if ($this->Auth->login()) {

                parent::redirectFilter();
            } else {
                $this->Session->setFlash(__('Usuario o password inválido'), 'default', array('class' => 'alert alert-danger'));
            }
        }
    }
    
    
    public function logout() {
	//$this->Session->destroy();
        $this->redirect($this->Auth->logout());
    }
    
    public function beforeFilter() {
        
        parent::beforeFilter();
        
        //solo pruebas (o al inicio del desarrollo), para poder creao o editar usuarios sin estar logueado
        //$this->Auth->allow('add', 'index', 'edit');
        
        $this->Auth->allow('login', 'logout');
        
        
        if( !parent::isAuthorized(AuthComponent::user()) )
            $this->Auth->deny('add', 'index', 'edit', 'view');
        else if( AuthComponent::user('webuser_profile_id') != '1'  ){//superadmin
            
            if( $this->request->params['action'] != 'login' && $this->request->params['action'] != 'logout' ){
            
                $this->Session->setFlash(__('No tienes permisos para realizar esa acción'), 'default', array('class' => 'alert alert-danger'));
                parent::redirectFilter();
            }
            
        }
    }
    
    /*
    public function beforeFilter() {
        
        parent::beforeFilter();
        
        //solo pruebas (o al inicio del desarrollo), para poder creao o editar usuarios sin estar logueado
        //$this->Auth->allow('add', 'index', 'edit');

        if( !parent::isAuthorized(AuthComponent::user()) )
            $this->Auth->deny('add', 'index', 'edit', 'view');
    }
    */

}