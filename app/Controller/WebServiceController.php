<?php

App::uses('AppController', 'Controller');

//App::uses('SimplePasswordHasher', 'Controller/Component/Auth');


/**
 * WebServices Controller
 *
 */
class WebServiceController extends AppController {

    public $components = array('RequestHandler'/* , 'Cookie' */);
    //private $configEmail = 'bluehost';


    /**
     *
     * @var mixed
     */

    /**
     * Parametros:
     *  GET:    
     *          $this->params["url"]["param1"]
     *          $this->request->query['param1']     ------------- usar este--------------
     *          $this->request->query('param1')
     * POST:   
     *          $this->request->data('param1')
     *          $this->request->data['MyModel']['param1']
     * 
     * $this->Model->recursive = -1;//will remove all associations
     * $this->Model->recursive = 0;//will remove only hasMany assosiation (so it keeps belongsTo)
     * 
     * NOTA: no usar isset() en los parametros, genera un error
     */
    public function beforeFilter() {
        parent::beforeFilter();

        $this->Auth->allow(
            'musers_login', //'drivers_logout', 'drivers_trackingps', 'drivers_getbills', 'drivers_billsdone', 'drivers_statistics', 'drivers_edit', 'drivers_getnovelties', 'drivers_getstatuses'
            'musers_getchallenges', 'musers_viewchallenge'

            //,'prueba'
            ,'prueba2'
            //, 'prueba_excel_to_json'
        );
    }
    
    public function prueba() {

        $success = true;
        $message = '';
        $data = NULL;

        $this->loadModel('WebUser');
        $this->WebUser->recursive = -1; //will remove all associations
        $webusers = $this->WebUser->find('all');
        
        foreach ($webusers as $key => $webuser) {
            
            $webusers[$key]['WebUser']['password2'] = AuthComponent::password($webuser['WebUser']['copiapassword']);
        }

        $data = $webusers;

        $response = array('success' => $success, 'message' => $message, 'data' => $data);
        $this->set(array('response' => $response, '_serialize' => array('response')));
    }
    
    
    public function prueba2() {

        $success = true;
        $message = '';
        $data = NULL;

        $desc = "Pago de Servicios Publicos";
        $txt = "Serviciox";
        
        $keyWordsBigData = explode(" ", strtolower($desc));
        
        $data = array( 
            'txt' => $txt,
            'desc' => $desc,
            'keyWordsBigData' => $keyWordsBigData,
            'searched' => array_key_exists(strtolower($txt), $keyWordsBigData),
            'searched2' => in_array(strtolower($txt), $keyWordsBigData),
            'searched3' => strpos(strtolower($desc), strtolower($txt))
        );

        $response = array('success' => $success, 'message' => $message, 'data' => $data);
        $this->set(array('response' => $response, '_serialize' => array('response')));
    }
    
    
    
    /**
     * Consultar todas las facturas con estado en 'En camion' de un determinado conductor
     */
    public function musers_getchallenges() {

        $success = false;
        $message = '';
        $data = NULL;

        if ( $this->request->data('muser_id')!='' ){

            $this->loadModel('MobileUser');
            $this->MobileUser->recursive = -1; //will remove all associations
            $muser = $this->MobileUser->findById($this->request->data('muser_id'));

            if ($muser != NULL && count($muser) > 0 && $muser['MobileUser'] != NULL) {

                if (strtoupper($muser['MobileUser']['activo']) == 'S') {

                    $this->loadModel('Challenge');

                    require_once(APP . 'Lib' . DS . 'Utils.php');

                    $this->Challenge->recursive = -1; //will remove all associations
                    $this->Challenge->Behaviors->load('Containable');
                    $this->Challenge->contain(
                        array( 
                            'Category' => array('fields' => array('Category.nombre', 'Category.imagen')),
                            'Company' => array('fields' => array('Company.nombre'))
                        )  
                    );
                    $challenges = $this->Challenge->find(
                            'all', array(
                                'conditions' => array(
                                    //'Challenge.muser_id' => $driver['Driver']['id'],
                                    'Challenge.fecha' => date('Y-m-d')
                                ),
                            'fields' => array(
                                'Challenge.id', 'Challenge.nombre', 'Challenge.fecha',
                                'Challenge.descripcion', 'Challenge.imagen',
                                //'Challenge.category_id', 'Challenge.company_id'
                            )
                        )
                    );

                    if ($challenges != NULL) {

                        $success = true;
                        $message = ( count($challenges) . " reto(s) encontrado(s)" );
                        $data = $challenges;
                    } else {
                        $success = false;
                        $message = "No se encontraron retos";
                    }
                }
            } else {

                $success = false;
                $message = 'No se encontró información de este usuario';
            }
        } else {
            $success = false;
            $message = 'Debe enviar el id del usuario';
        }

        $response = array('success' => $success, 'message' => $message, 'data' => $data);
        $this->set(array('response' => $response, '_serialize' => array('response')));
    }
    
    
    public function musers_viewchallenge() {

        $success = false;
        $message = '';
        $data = NULL;

        if ( $this->request->data('muser_id')!='' && $this->request->data('challenge_id')!='' ){

            $this->loadModel('MobileUser');
            $this->MobileUser->recursive = -1; //will remove all associations
            $muser = $this->MobileUser->findById($this->request->data('muser_id'));

            if ($muser != NULL && count($muser) > 0 && $muser['MobileUser'] != NULL) {

                if (strtoupper($muser['MobileUser']['activo']) == 'S') {

                    $this->loadModel('Challenge');

                    require_once(APP . 'Lib' . DS . 'Utils.php');

                    $this->Challenge->recursive = -1; //will remove all associations
                    $this->Challenge->Behaviors->load('Containable');
                    $this->Challenge->contain(
                        array( 
                            'Category' => array('fields' => array('Category.nombre', 'Category.imagen')),
                            'Company' => array('fields' => array('Company.nombre'))
                        )  
                    );
                    $challenge = $this->Challenge->find(
                            'first', array(
                                'conditions' => array(
                                    'Challenge.id' => $this->request->data('challenge_id')
                                ),
                            'fields' => array(
                                'Challenge.id', 'Challenge.nombre', 'Challenge.fecha',
                                'Challenge.descripcion', 'Challenge.imagen',
                            )
                        )
                    );

                    if ($challenge != NULL) {

                        $success = true;
                        $message = "Información del reto";
                        $data = $challenge;
                    } else {
                        $success = false;
                        $message = "No se encontró el reto";
                    }
                }
            } else {

                $success = false;
                $message = 'No se encontró información de este usuario';
            }
        } else {
            $success = false;
            $message = 'Debe enviar el id del usuario';
        }

        $response = array('success' => $success, 'message' => $message, 'data' => $data);
        $this->set(array('response' => $response, '_serialize' => array('response')));
    }
    
    
    
    
    
    
    
    
    
    

    /**
     * Login de usuario movil
     * params: email y password
     * return: response con objeto MobileUser
     */
    public function drivers_login() {

        $success = false;
        $message = 'No se pudo hacer el login, por favor intente de nuevo';
        $muser = NULL;

        if (($this->request->data("user") != '') && ($this->request->data("password") != '')) {

            //aplicar AuthComponent::password(password) si se almaceno encriptado el password del usuario movil

            $this->loadModel('Driver');
            $this->Driver->recursive = -1; //para quitar todas las clases asociadas
            $muser = $this->Driver->find('first', array(
                'conditions' => array(
                    'Driver.user' => $this->request->data('user'),
                    'Driver.password' => $this->request->data('password')
                )
                    )
            );

            if ($muser != NULL && count($muser) > 0) {

                unset($muser['Driver']['password']);  // don't forget this part

                $this->loadModel('District');
                $this->District->recursive = -1; //para quitar todas las clases asociadas
                $district = $this->District->findById($muser['Driver']['district_id']);

                if ($district != NULL && count($district) > 0)
                    $muser['Driver']['cierrexsistemas'] = $district['District']['cierrexsistemas'];

                /*
                  {
                  //fecha actual + 6 horas
                  $fechatokenvalido = date('Y-m-d H:i:s', strtotime('+6 hour', strtotime(date('Y-m-d H:i:s'))));

                  $token = String::uuid();

                  $this->MobileUser->set("token", $token);

                  $data = array(
                  'id' => $muser["MobileUser"]["id"],
                  'token' => $token,
                  'fechatokenvalido' => $fechatokenvalido
                  );

                  if( $this->MobileUser->save($data) )
                  $muser['MobileUser']['token'] = $token;
                  else{

                  $token = md5(uniqid(rand(), true));

                  $data['token'] = $token;

                  if( $this->MobileUser->save() )
                  $muser['MobileUser']['token'] = $token;
                  }
                  }
                 */

                $success = true;
                $message = 'Login exitoso!'; {
                    $this->loadModel('Driver');
                    $this->Driver->recursive = -1; //para quitar todas las clases asociadas
                    $data = array(
                        'id' => $muser["Driver"]["id"],
                        'fechaultimologin' => date('Y-m-d H:i:s')
                    );
                    $this->Driver->save($data);
                }
            } else {

                $muser = NULL;
                $success = false;
                $message = 'Usuario y/o password inválidos';
            }
        } else {
            $success = false;
            $message = 'Debe enviar el usuario y password';
        }

        $response = array('success' => $success, 'message' => $message, 'data' => $muser);
        $this->set(array('response' => $response, '_serialize' => array('response')));
    }

    public function drivers_logout() {

        $success = false;
        $message = 'No se pudo hacer logout, por favor intente de nuevo';
        $muser = NULL;

        if (($this->request->data("driver_id") != '')) {

            //aplicar AuthComponent::password(password) si se almaceno encriptado el password del usuario movil

            $this->loadModel('Driver');
            $this->Driver->recursive = -1; //para quitar todas las clases asociadas
            $data = array(
                'id' => $this->request->data("driver_id"),
                'fechaultimodeslogueo' => date('Y-m-d H:i:s'),
                'tokenpush' => ''
            );

            if ($this->Driver->save($data)) {

                $success = true;
                $message = 'Logout exitoso!';
            } else {

                $muser = NULL;
                $success = false;
                $message = 'No se pudo hacer el logout';
            }
        } else {
            $success = false;
            $message = 'Debe enviar el id del conductor';
        }

        $response = array('success' => $success, 'message' => $message, 'data' => $muser);
        $this->set(array('response' => $response, '_serialize' => array('response')));
    }

    /**
     * Consultar todas las facturas con estado en 'En camion' de un determinado conductor
     */
    public function drivers_getbills() {

        $success = false;
        $message = '';
        $data = NULL;

        if ((/* $this->request->data('driver_id') != NULL && */
                $this->request->data('driver_id') != '')) {

            $this->loadModel('Driver');
            $this->Driver->recursive = -1; //will remove all associations
            $driver = $this->Driver->findById($this->request->data('driver_id'));

            if ($driver != NULL && count($driver) > 0 && $driver['Driver'] != NULL) {

                if (strtoupper($driver['Driver']['activo']) == 'S') {

                    $this->loadModel('Bill');

                    require_once(APP . 'Lib' . DS . 'Utils.php');

                    $this->loadModel('Status');
                    $this->Status->recursive = -1;
                    $statuses = $this->Status->find('all', array('fields' => array('id', 'nombre')));
                    $statusencamion_id = findByNameInModel('camion', false, 'Status', 'nombre', $statuses);

                    $this->Bill->recursive = -1; //will remove all associations
                    $bills = $this->Bill->find(
                            'all', array(
                        'conditions' => array(
                            'Bill.driver_id' => $driver['Driver']['id'],
                            'Bill.status_id' => $statusencamion_id
                        ),
                        'fields' => array(
                            'Bill.id', 'Bill.nombrecliente AS cliente', 'Bill.numerofactura AS factura',
                            'Bill.numeroplanilla AS planilla', 'Bill.direccioncliente AS direccion',
                            'Bill.fechafactura', 'Bill.identificacioncliente AS nit', 'Bill.condicion'
                        )
                            )
                    );

                    if ($bills != NULL) {

                        $success = true;
                        $message = ( count($bills) . " factura(s) encontrada(s)" );
                        $data = $bills;
                    } else {
                        $success = false;
                        $message = "No se encontraron facturas";
                    }
                }
            } else {

                $success = false;
                $message = 'No se encontró información de este conductor';
            }
        } else {
            $success = false;
            $message = 'Debe enviar el id del conductor';
        }

        $response = array('success' => $success, 'message' => $message, 'data' => $data);
        $this->set(array('response' => $response, '_serialize' => array('response')));
    }

    /**
     * End Point para recibir las facturas cumplidas o novedades de las mismas
     * Sirve tanto para envíos normales como para los re-envíos de las facturas/novedades
     * que quedaron en modo offline en las apps de los conductores
     * Si ( "fechaenvio" ) => es un re-envío
     * Sino => es envío normal
     * envio mencionada, a diferencia de la fecha hora actual para los envíos normales
     * 
     */
    public function drivers_billsdone() {

        $success = false;
        $message = '';
        $data = NULL;

        if ((/* $this->request->data('driver_id')!=NULL && */
                $this->request->data('driver_id') != '') &&
                (/* $this->request->data('bills')!=NULL && */
                $this->request->data('bills') != '') &&
                (/* $this->request->data('latitude')!=NULL && */
                $this->request->data('latitude') != '') &&
                (/* $this->request->data('longitude')!=NULL && */
                $this->request->data('longitude') != '')
        ) {

            $this->loadModel('Driver');
            $this->Driver->recursive = -1; //will remove all associations
            $driver = $this->Driver->findById($this->request->data('driver_id'));

            if ($driver != NULL && count($driver) > 0 && $driver['Driver'] != NULL) {

                if (strtoupper($driver['Driver']['activo']) == 'S') {
                    
                    $urlbillsdone = NULL;
                    {
                        $this->loadModel('Company');
                        $this->Company->recursive = -1; //will remove all associations
                        $company = $this->Company->findById($driver['Driver']['company_id']);
                        if( $company!=NULL && count($company)>0 )
                            $urlbillsdone = $company['Company']['urlbillsdone'];
                    }

                    $novelty_id = NULL;

                    require_once(APP . 'Lib' . DS . 'Utils.php');

                    //$novedad = '';
                    if ($this->request->data('novelty_id') != '')
                        $novelty_id = trim($this->request->data('novelty_id'));
                    else if ($this->request->data('novelty') != '') {

                        $this->loadModel('Novelty');
                        $this->Novelty->recursive = -1;
                        $novedades = $this->Novelty->find('all');

                        $novelty_id = findByNameInModel(trim($this->request->data('novelty')), false, 'Novelty', 'nombre', $novedades);
                    }

                    $bills = $this->request->data('bills');

                    $arrayBillIds = split(",", $bills);

                    $this->loadModel('Bill');
                    $this->Bill->recursive = -1; //will remove all associations
                    //date_default_timezone_set('Etc/GMT' . '-5');
                    date_default_timezone_set('America/Bogota');

                    $this->loadModel('Status');
                    $this->Status->recursive = -1;
                    $statuses = $this->Status->find('all');

                    $statusnovedad_id = findByNameInModel('novedad', false, 'Status', 'nombre', $statuses);
                    $statusencamion_id = findByNameInModel('camion', false, 'Status', 'nombre', $statuses);
                    $statuscumplido_id = findByNameInModel('cumplido', false, 'Status', 'nombre', $statuses);


                    if ($arrayBillIds != NULL && count($arrayBillIds) > 0) {

                        $cont = 0;

                        for ($i = 0; $i < count($arrayBillIds); $i++) {

                            if ($arrayBillIds[$i] != NULL && $arrayBillIds[$i] != '') {

                                $esReenvio = false;

                                //para los re-envíos
                                if ($this->request->data('fechaenvio') != NULL &&
                                        $this->request->data('fechaenvio') != '') {

                                    $fecha = $this->request->data('fechaenvio');
                                    $esReenvio = true;
                                } else
                                    $fecha = date('Y-m-d H:i:s');

                                $dataBills = array(
                                    "id" => $arrayBillIds[$i],
                                    "fechaentrega" => $fecha,
                                    "fecha_entrega" => date('Y-m-d', strtotime($fecha)),
                                    "hora_entrega" => date('H:i:s', strtotime($fecha)),
                                    "latitud" => $this->request->data('latitude'),
                                    "longitud" => $this->request->data('longitude')
                                );

                                if ($novelty_id != NULL && $novelty_id != '') {

                                    $dataBills["novelty_id"] = $novelty_id;
                                    $dataBills["status_id"] = $statusnovedad_id;
                                } else
                                    $dataBills["status_id"] = $statuscumplido_id;

                                $bill = $this->Bill->findById($arrayBillIds[$i]);

                                if ($bill != NULL) {

                                    if ($bill['Bill']['status_id'] === $statusencamion_id) {

                                        try {
                                            $fecha1 = new DateTime($bill['Bill']["fechaplanilla"]);
                                            $fecha2 = new DateTime($fecha);
                                            $diff = $fecha1->diff($fecha2);

                                            $deltaHoras = 0;
                                            if ($diff->d > 0)//dias
                                                $deltaHoras = ( 24 * $diff->d);
                                            
                                            $dataBills["transcurrido_entrega"] = ( ($diff->h + $deltaHoras) . ':' . $diff->i . ':' . $diff->s);
                                        } catch (Exception $exc) { }

                                        try {
                                            $fecha1 = new DateTime($bill['Bill']["fechafactura"]);
                                            $fecha2 = new DateTime($fecha);
                                            $diff = $fecha1->diff($fecha2);

                                            $deltaHoras = 0;
                                            if ($diff->d > 0) {//dias
                                                $deltaHoras = ( 24 * $diff->d);
                                            }

                                            $dataBills["transcurrido_total"] = ( ($diff->h + $deltaHoras) . ':' . $diff->i . ':' . $diff->s);
                                        } catch (Exception $exc) { }

                                        /*
                                          if( $esReenvio==true ){

                                          //if( $novedad==NULL || $novedad=='' )
                                          if( $novelty_id==NULL || $novelty_id=='' )
                                          $dataBills["motivonovedad"] = '';

                                          if( ($bill['Bill']["motivonovedad"]!=NULL &&
                                          $bill['Bill']["motivonovedad"]!='') ){

                                          $dataBills["motivonovedad"] = (
                                          $bill['Bill']['motivonovedad'].
                                          '<br/>Fecha recibido: '.date('Y-m-d H:i:s').'<br/>'.
                                          $dataBills["motivonovedad"]
                                          );
                                          }
                                          }
                                         */
                                    } else //factura ya gestionada, no se puede dar cumplido
                                        $dataBills = NULL;
                                }

                                if ($dataBills != NULL) {
                                    
                                    if ($this->Bill->save($dataBills)){
                                        $cont++;
                                        
                                        /*
                                        $type = "Cumplido";
                                        if ($novelty_id!=NULL && $novelty_id!='')
                                            $type = "Novedad";
                                        
                                        
                                        $this->loadModel('Bill');
                                        $this->Bill->recursive = -1;
                                        $billInfo = $this->Bill->find('first', array('conditions' => array('id' => $arrayBillIds[$i]), 'fields' => 'numerofactura', 'numeroplanilla'));
                                        
                                        
                                        //require_once(APP . 'Lib' . DS . 'Utils.php');
                                        $json = httpConnectionToJson( 
                                            $urlbillsdone,
                                            "POST",
                                            NULL,
                                            array( "fechahora" => $fecha, "estado" => $type, "numerofactura" => $billInfo['Bill']['numerofactura'] )
                                        );
                                        */
                                    }
                                }
                            }
                        }//end for
                    }

                    $type = "factura(s) cumplida(s)";
                    //if( $novedad!=NULL && $novedad!='' )
                    if ($novelty_id != NULL && $novelty_id != '')
                        $type = "novedad(es) recibida(s)";

                    $message = ($cont . " " . $type . " exitosamente");
                    $success = true;
                }else {

                    $success = false;
                    $message = "Este conductor está deshabilitado";
                }
            } else {

                $success = false;
                $message = "No se encontró información de este conductor";
            }
        } else {
            $success = false;
            $message = "Debe enviar el id del conductor, las facturas y las coordenadas gps";
        }

        $response = array('success' => $success, 'message' => $message, 'data' => $data);
        $this->set(array('response' => $response, '_serialize' => array('response')));
    }

    /**
     * Guardar tracking gps del conductor
     */
    public function drivers_trackingps() {

        $success = false;
        $message = 'No se pudo guardar el gps, por favor intente de nuevo';
        $data = NULL;

        if ((/* $this->request->data("driver_id")!=null && */
                $this->request->data("driver_id") != '') &&
                //($this->request->data("bill_id")!=null && $this->request->data("bill_id")!='') &&
                (/* $this->request->data("latitude")!=null && */
                $this->request->data("latitude") != '') &&
                (/* $this->request->data("longitude")!=null && */
                $this->request->data("longitude") != '')) {


            $this->loadModel('Driver');
            $this->Driver->recursive = -1; //will remove all associations
            $driver = $this->Driver->findById($this->request->data('driver_id'));

            require_once(APP . 'Lib' . DS . 'Utils.php');

            if ($driver != NULL && count($driver) > 0 && $driver['Driver'] != NULL) {

                if (strtoupper($driver['Driver']['activo']) == 'S') {

                    date_default_timezone_set('America/Bogota');

                    $datosTrackingGps = array();
                    $datosTrackingGps['driver_id'] = $this->request->data("driver_id");
                    $datosTrackingGps['coordenadas'] = '';
                    $datosTrackingGps['horas'] = '';
                    $datosTrackingGps['ultimahora'] = date('H:i:s');


                    $datosTrackingGps['ultimacoordenada'] = (
                            $this->request->data("latitude") . $tokenCoordinatesBetween . $this->request->data("longitude")
                            );

                    //solo pruebas: para ver cada cuanto envía el gps el telefono
                    //$datosTrackingGps['fechas'] = '';

                    $this->loadModel('GpsTracking');

                    $todayTracking = $this->GpsTracking->find('first', array(
                        'conditions' => array(
                            'GpsTracking.fecha' => date('Y-m-d'),
                            'driver_id' => $this->request->data("driver_id")
                        )
                    ));

                    if ($todayTracking != NULL && count($todayTracking) > 0) {

                        $datosTrackingGps['id'] = $todayTracking['GpsTracking']['id'];
                    } else {
                        $datosTrackingGps['fecha'] = date('Y-m-d');
                        $datosTrackingGps['primerahora'] = date('H:i:s');
                        $datosTrackingGps['primeracoordenada'] = (
                                $this->request->data("latitude") . $tokenCoordinatesBetween . $this->request->data("longitude")
                                );
                    }

                    $this->loadModel('GpsTracking');

                    if ($this->GpsTracking->save($datosTrackingGps)) {

                        $success = true;
                        $message = 'Gps guardado!';
                    } else {

                        $success = false;
                        $message = 'No se pudo guardar el gps';
                    }
                } else {

                    $success = false;
                    $message = "Esta cuenta de conductor está deshabilitada";
                }
            } else {

                $success = false;
                $message = "No se encontró información de este conductor";
            }
        } else {
            $success = false;
            $message = 'Debe enviar las coordenadas gps y los datos del conductor';
        }

        $response = array('success' => $success, 'message' => $message, 'data' => $data);
        $this->set(array('response' => $response, '_serialize' => array('response')));
    }

    /**
     * Estadisticas del día del conductor
     */
    public function drivers_statistics() {

        $success = false;
        $message = 'No se pudo hacer el login, por favor intente de nuevo';
        $data = NULL;

        if (($this->request->data("driver_id") != '')) {

            date_default_timezone_set('America/Bogota');
            require_once(APP . 'Lib' . DS . 'Utils.php');

            $this->loadModel('Status');
            $this->Status->recursive = -1;
            $statuses = $this->Status->find('all', array('fields' => array('id', 'nombre')));
            $statusencamion_id = findByNameInModel('camion', false, 'Status', 'nombre', $statuses);
            $statusnovedad_id = findByNameInModel('novedad', true, 'Status', 'nombre', $statuses);
            $statuscumplido_id = findByNameInModel('cumplido', true, 'Status', 'nombre', $statuses);

            $this->loadModel('Driver');
            $this->Driver->recursive = -1; //will remove all associations
            $driver = $this->Driver->findById($this->request->data("driver_id"));

            if ($driver != NULL && count($driver) > 0) {

                $this->loadModel('Bill');

                $this->Bill->recursive = -1; //will remove all associations
                $totalBills = $this->Bill->find('all', array(
                    'fields' => array('COUNT(*) AS total'),
                    'conditions' => array(
                        'Bill.driver_id' => $driver['Driver']['id'],
                        'Bill.status_id' => $statusencamion_id
                    )
                ));

                if ($totalBills != NULL)
                    $totalBills = $totalBills[0][0]['total'];
                else
                    $totalBills = 0;

                $this->Bill->recursive = -1; //will remove all associations
                $totalNoveltyBills = $this->Bill->find('all', array(
                    'fields' => array('COUNT(*) AS total'),
                    'conditions' => array(
                        'Bill.driver_id' => $driver['Driver']['id'],
                        'Bill.status_id' => $statusnovedad_id,
                        'Bill.fechaplanilla >= ' => ( date('Y-m-d') . ' 00:00:00' )
                    )
                ));

                if ($totalNoveltyBills != NULL)
                    $totalNoveltyBills = $totalNoveltyBills[0][0]['total'];
                else
                    $totalNoveltyBills = 0;

                $this->Bill->recursive = -1; //will remove all associations
                $totalDeliveredBills = $this->Bill->find('all', array(
                    'fields' => array('COUNT(*) AS total'),
                    'conditions' => array(
                        'Bill.driver_id' => $driver['Driver']['id'],
                        'Bill.status_id' => $statuscumplido_id,
                        'Bill.fechaplanilla >= ' => ( date('Y-m-d') . ' 00:00:00' )
                    )
                ));

                if ($totalDeliveredBills != NULL)
                    $totalDeliveredBills = $totalDeliveredBills[0][0]['total'];
                else
                    $totalDeliveredBills = 0;

                $message = "Estadistica del día";

                $data = array(
                    "pendientes" => $totalBills,
                    "novedades" => $totalNoveltyBills,
                    "cumplidas" => $totalDeliveredBills
                );
            }
        }else {
            $success = false;
            $message = 'Debe enviar el id del conductor';
        }

        $response = array('success' => $success, 'message' => $message, 'data' => $data);
        $this->set(array('response' => $response, '_serialize' => array('response')));
    }

    public function drivers_edit() {

        $success = false;
        $message = '';
        $data = NULL;

        if ($this->request->data('driver_id') != '') {

            $this->loadModel('Driver');
            $this->Driver->recursive = -1; //will remove all associations
            $driver = $this->Driver->findById($this->request->data('driver_id'));

            if ($driver != NULL && count($driver) > 0 && $driver['Driver'] != NULL) {

                if (strtoupper($driver['Driver']['activo']) == 'S') {

                    $dataDriver = array("id" => $this->request->data('driver_id'));

                    if (/* $this->request->data('novelty')!=NULL && */
                            $this->request->data('tokenpush') != '')
                        $dataDriver['tokenpush'] = $this->request->data('tokenpush');


                    $this->loadModel('Driver');
                    $this->Driver->recursive = -1; //will remove all associations

                    if ($this->Driver->save($dataDriver)) {

                        $message = "Datos del conductor guardados exitosamente";
                        $success = true;

                        $this->loadModel('Driver');
                        $this->Driver->recursive = -1; //will remove all associations
                        $data = $this->Driver->findById($this->request->data('driver_id'));

                        if ($data != NULL && count($data) > 0) {

                            unset($data['Driver']['password']);

                            $this->loadModel('District');
                            $this->District->recursive = -1; //para quitar todas las clases asociadas
                            $district = $this->District->findById($data['Driver']['district_id']);

                            if ($district != NULL && count($district) > 0)
                                $data['Driver']['cierrexsistemas'] = $district['District']['cierrexsistemas'];
                        }
                    }else {

                        $message = ("No se pudieron guardar los datos del conductor");
                        $success = false;
                    }
                } else {

                    $success = false;
                    $message = "Este conductor está deshabilitado";
                }
            } else {

                $success = false;
                $message = "No se encontró información de este conductor";
            }
        } else {
            $success = false;
            $message = "Debe enviar el id del conductor, las facturas y las coordenadas gps";
        }

        $response = array('success' => $success, 'message' => $message, 'data' => $data);
        $this->set(array('response' => $response, '_serialize' => array('response')));
    }

    /**
     * Listado de novedades
     */
    public function drivers_getnovelties() {

        $success = false;
        $message = 'No se pudieon consultar las novedades';
        $data = NULL;

        if (($this->request->data("driver_id") != '')) {

            $this->loadModel('Driver');
            $this->Driver->recursive = -1; //will remove all associations
            $driver = $this->Driver->findById($this->request->data("driver_id"));

            if ($driver != NULL && count($driver) > 0 && $driver['Driver'] &&
                    $driver['Driver'] != NULL) {

                if (strtoupper($driver['Driver']['activo']) == 'S') {

                    $this->loadModel('Novelty');
                    $this->Novelty->recursive = -1; //will remove all associations
                    $novelties = $this->Novelty->find('all', array('order' => 'Novelty.nombre ASC'));

                    if ($novelties != NULL && count($novelties) > 0) {

                        $success = true;
                        $data = $novelties;
                        $message = "Listado de novedades";
                    } else {

                        $success = false;
                        $message = "No se encontraron novedades";
                    }
                } else {

                    $success = false;
                    $message = "Este conductor se encuentra desactivado";
                }
            } else {

                $success = false;
                $message = "No se encontró información del conductor";
            }
        } else {
            $success = false;
            $message = 'Debe enviar el id del conductor';
        }

        $response = array('success' => $success, 'message' => $message, 'data' => $data);
        $this->set(array('response' => $response, '_serialize' => array('response')));
    }

    /**
     * Listado de estados
     */
    public function drivers_getstatuses() {

        $success = false;
        $message = 'No se pudieon consultar los estados';
        $data = NULL;

        if (($this->request->data("driver_id") != '')) {

            $this->loadModel('Driver');
            $this->Driver->recursive = -1; //will remove all associations
            $driver = $this->Driver->findById($this->request->data("driver_id"));

            if ($driver != NULL && count($driver) > 0 && $driver['Driver'] &&
                    $driver['Driver'] != NULL) {

                if (strtoupper($driver['Driver']['activo']) == 'S') {

                    $this->loadModel('Status');
                    $this->Status->recursive = -1; //will remove all associations
                    $status = $this->Status->find('all'); //, array('order' => 'Status.nombre ASC'));

                    if ($status != NULL && count($status) > 0) {

                        $success = true;
                        $data = $status;
                        $message = "Listado de estados";
                    } else {

                        $success = false;
                        $message = "No se encontraron estados";
                    }
                } else {

                    $success = false;
                    $message = "Este conductor se encuentra desactivado";
                }
            } else {

                $success = false;
                $message = "No se encontró información del conductor";
            }
        } else {
            $success = false;
            $message = 'Debe enviar el id del conductor';
        }

        $response = array('success' => $success, 'message' => $message, 'data' => $data);
        $this->set(array('response' => $response, '_serialize' => array('response')));
    }

    /*
    public function prueba_excel_to_json() {

        $success = true;
        $message = '';
        $data = NULL;

        require_once(APP . 'Vendor' . DS . 'PHPExcel/PHPExcel/IOFactory.php');

        $path = "data.xlsx";
        $fileObj = PHPExcel_IOFactory::load($path);
        //$sheetObj = $fileObj->getActiveSheet();
        //$highestColumm = $fileObj->setActiveSheetIndex(0)->getHighestColumn();
        $highestRow = $fileObj->setActiveSheetIndex(0)->getHighestRow();

        //$number_of_columns = $highestColumm;
        for ($row = 1; $row < $highestRow + 1; $row++) {
            $dataset = array();

            $column = 0;
            $columnValue = $columnValue = $fileObj->setActiveSheetIndex(0)->getCellByColumnAndRow($column, $row)->getFormattedValue();
            while ($columnValue != NULL) {
                $dataset[] = $columnValue;
                $column++;
                $columnValue = $fileObj->setActiveSheetIndex(0)->getCellByColumnAndRow($column, $row)->getFormattedValue();
            }

            $datasets[] = $dataset;
        }

        $header = $datasets[0];

        $jsonData = array();
        $blocco = array();

        for ($i = 1; $i < count($datasets); $i++) {

            $parte = array();

            $parte = $datasets[$i];

            $j = 0;
            foreach ($header as $col) {
                $blocco[$col] = $parte[$j];
                $j++;
            }
            $jsonData[] = $blocco;

            unset($parte);
        }

        $data = $jsonData;

        $response = array('success' => $success, 'message' => $message, 'data' => $data);
        $this->set(array('response' => $response, '_serialize' => array('response')));
    }
    */
    

}
