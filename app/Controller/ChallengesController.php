<?php

App::uses('AppController', 'Controller');

/**
 * Challenges Controller
 *
 * @property Challenge $Challenge
 * @property PaginatorComponent $Paginator
 */
class ChallengesController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator');

    /**
     * index method
     *
     * @return void
     */
    public function index() {
        
        $conditionCompany = '';
        if( AuthComponent::user('company_id') && 
                AuthComponent::user('company_id')!=NULL &&
                AuthComponent::user('company_id')!='' &&
                AuthComponent::user('company_id')!='0' && 
                AuthComponent::user('company_id')!=0 ){

            $conditionCompany = array( 'company_id' => AuthComponent::user('company_id') );
        }
        
        $this->paginate = array(
            'conditions' => $conditionCompany,
        );
        
        $this->Challenge->recursive = 0;
        $this->set('challenges', $this->Paginator->paginate());
    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null) {
        if (!$this->Challenge->exists($id)) {
            throw new NotFoundException(__('Reto inválido'));
        }
        $options = array('conditions' => array('Challenge.' . $this->Challenge->primaryKey => $id));
        $this->set('challenge', $this->Challenge->find('first', $options));
    }

    /**
     * add method
     *
     * @return void
     */
    public function add() {
        if ($this->request->is('post')) {
            $this->Challenge->create();
            if ($this->Challenge->save($this->request->data)) {
                $this->Session->setFlash(__('Reto guardado exitosamente.'), 'default', array('class' => 'alert alert-success'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('El reto no pudo ser guardado. Por favor, intente de nuevo.'), 'default', array('class' => 'alert alert-danger'));
            }
        }
        
        $conditionCompany = '';
        if( AuthComponent::user('company_id') && 
                AuthComponent::user('company_id')!=NULL &&
                AuthComponent::user('company_id')!='' &&
                AuthComponent::user('company_id')!='0' && 
                AuthComponent::user('company_id')!=0 ){

            $conditionCompany = array( 'id' => AuthComponent::user('company_id') );
        }
        
        //echo '<br/><br/><br/>'.json_encode($conditionCompany);
        
        $companies = $this->Challenge->Company->find('list', array('fields' => array('id', 'nombre'), 'conditions' => $conditionCompany, 'order' => 'nombre asc'));
        
        $categories = $this->Challenge->Category->find('list', array('fields' => array('id', 'nombre'), 'order' => 'nombre asc'));
        
        $this->set(compact('companies', 'categories'));
    }

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null) {
        if (!$this->Challenge->exists($id)) {
            throw new NotFoundException(__('Reto inválido'));
        }
        if ($this->request->is(array('post', 'put'))) {
            if ($this->Challenge->save($this->request->data)) {
                $this->Session->setFlash(__('Reto guardado exitosamente.'), 'default', array('class' => 'alert alert-success'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('El reto no pudo ser guardado. Por favor, intente de nuevo.'), 'default', array('class' => 'alert alert-danger'));
            }
        } else {
            $options = array('conditions' => array('Challenge.' . $this->Challenge->primaryKey => $id));
            $this->request->data = $this->Challenge->find('first', $options);
        }
        
        $conditionCompany = '';
        if( AuthComponent::user('company_id') && 
                AuthComponent::user('company_id')!=NULL &&
                AuthComponent::user('company_id')!='' &&
                AuthComponent::user('company_id')!='0' && 
                AuthComponent::user('company_id')!=0 ){

            $conditionCompany = array( 'id' => AuthComponent::user('company_id') );
        }
        
        
        $companies = $this->Challenge->Company->find('list', array('fields' => array('id', 'nombre'), 'conditions' => $conditionCompany, 'order' => 'nombre asc'));
        
        $categories = $this->Challenge->Category->find('list', array('fields' => array('id', 'nombre'), 'order' => 'nombre asc'));
        
        $this->set(compact('companies', 'categories'));
    }

    /**
     * delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function delete($id = null) {
        $this->Challenge->id = $id;
        if (!$this->Challenge->exists()) {
            throw new NotFoundException(__('Reto inválido'));
        }
        $this->request->allowMethod('post', 'delete');
        if ($this->Challenge->delete()) {
            $this->Session->setFlash(__('Reto borrado exitosamente.'), 'default', array('class' => 'alert alert-success'));
        } else {
            $this->Session->setFlash(__('El reto no pudo ser borrado. Por favor, intente de nuevo.'), 'default', array('class' => 'alert alert-danger'));
        }
        return $this->redirect(array('action' => 'index'));
    }

}
