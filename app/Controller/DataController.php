<?php

App::uses('AppController', 'Controller');

/**
 * Data Controller
 *
 * @property Data $Data
 * @property PaginatorComponent $Paginator
 */
class DataController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator');

    /**
     * index method
     *
     * @return void
     */
    public function index() {
        $this->Data->recursive = 0;
        $this->set('data', $this->Paginator->paginate());
    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null) {
        if (!$this->Data->exists($id)) {
            throw new NotFoundException(__('Data inválida'));
        }
        $options = array('conditions' => array('Data.' . $this->Data->primaryKey => $id));
        $this->set('data', $this->Data->find('first', $options));
    }

    /**
     * add method
     *
     * @return void
     */
    public function add() {
        if ($this->request->is('post')) {
            $this->Data->create();
            if ($this->Data->save($this->request->data)) {
                $this->Session->setFlash(__('Data guardada exitosamente.'), 'default', array('class' => 'alert alert-success'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('La data no pudo ser guardada. Por favor, intente de nuevo.'), 'default', array('class' => 'alert alert-danger'));
            }
        }
    }

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null) {
        if (!$this->Data->exists($id)) {
            throw new NotFoundException(__('Data inválida'));
        }
        if ($this->request->is(array('post', 'put'))) {
            if ($this->Data->save($this->request->data)) {
                $this->Session->setFlash(__('Data guardada exitosamente.'), 'default', array('class' => 'alert alert-success'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('La data no pudo ser guardada. Por favor, intente de nuevo.'), 'default', array('class' => 'alert alert-danger'));
            }
        } else {
            $options = array('conditions' => array('Data.' . $this->Data->primaryKey => $id));
            $this->request->data = $this->Data->find('first', $options);
        }
    }

    /**
     * import method
     *
     * @return void
     */
    public function import($company_id = NULL) {

        if ($this->request->is('post')) {

            date_default_timezone_set('America/Bogota');

            include_once(APP . 'Lib' . DS . 'Utils.php');

            //$columnName = 'csvfile';
            $columnName = 'excelfile';

            //echo '<br/><br/><br/>'.json_encode($this->request->data);return;

            if (isset($this->request->data['Data'][$columnName]/* $_FILES['csvfile'] */)) {

                $ext = explode(".", $this->request->data['Data'][$columnName]['name'])[1];

                if ($ext != 'xls' && $ext != 'xlsx')
                    $this->Session->setFlash(__('Archivo no permitido, debe ser un archivo .xls o .xlsx'), 'default', array('class' => 'alert alert-danger'));
                else {

                    //echo '<br/><br/><br/><br/>';

                    $json = excel2Json($this->request->data['Data'][$columnName]['tmp_name']);
                    
                    //echo '<br/><br/><br/><br/>'.count($json);//.$json;
                    
                    if( $json!=NULL ){
                        
                        //$json = json_encode($json);
                    
                        $cont = 0;
                        foreach ($json as $value) {
                        //for ($i = 0; $ $i < count($json); $i++) {
                            
                            //$value = $json[$i];
                            
                            //echo '<br/>'.json_encode($value);

                            $arrayData = array( 
                                'telefono' => $value['telefono'],
                                'descripcion' => ucfirst($value['descripcion']),
                                'fecha' => $value['fecha'],
                                'valor' => $value['valor']
                            );

                            
                            $this->Data->create();
                            if ($this->Data->saveAll($arrayData))
                                $cont++;
                            
                            
                        }

                        $this->Session->setFlash(__('Se guardaron '.$cont.' datos'), 'default', array('class' => 'alert alert-success'));
                        return $this->redirect(array('action' => 'index'));

                        /*
                          $maindir = ('files' . DS . 'companies' . DS);
                          $dir = ( $maindir . trim($this->request->data['Data']['company_id']) . DS );

                          // files/carddata/
                          if (!is_dir($maindir))
                          mkdir($maindir);

                          // files/carddata/id/
                          if (!is_dir($dir))
                          mkdir($dir);

                          //$nombre = (date('Y-m-d')."_".date('H:i:s'));
                          $nombre = (date('YmdHis'));
                          $nombrearchivo = ( $nombre . ".json");

                          $file = new File($dir . $nombrearchivo, true, 0644);
                          //echo '<br/><br/><br/><br/>'.$file->write( ($json) );
                          $file->write(($json));
                          $file->close();
                         */

                        //$this->Session->setFlash(__('Data cargada exitosamente. Debes esperar unos minutos para que nuestro Bot la analice'), 'default', array('class' => 'alert alert-success'));
                        //return $this->redirect(array('action' => 'index'));
                    }
                }
            }

            /*
              if ($this->Bill->save($this->request->data)) {

              $this->Session->setFlash(__('Facturas guardadas exitosamente'), 'default', array('class' => 'alert alert-success'));
              return $this->redirect(array('action' => 'index'));
              } else {
              $this->Session->setFlash(__('La factura no pudo ser guardada. Por favor, intente de nuevo.'), 'default', array('class' => 'alert alert-danger'));
              }
             */
        }

        $filtroCompany = NULL;

        if (AuthComponent::user('company_id') &&
                AuthComponent::user('company_id') != NULL &&
                AuthComponent::user('company_id') != '' &&
                AuthComponent::user('company_id') != '0') {

            $filtroCompany = array(
                'id' => AuthComponent::user('company_id')
            );
        }

        if ($company_id != NULL && $company_id != '') {

            $filtroCompany = array(
                'id' => $company_id
            );
        }

        $this->loadModel('Company');
        $this->Company->recursive = -1;
        $companies = $this->Company->find('list', array(
                'fields' => array('id', 'nombre'),
                'conditions' => $filtroCompany,
                'order' => 'nombre ASC'
            )
        );

        $this->set(compact('companies', 'company_id'));
    }

    /**
     * delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function delete($id = null) {
        $this->Data->id = $id;
        if (!$this->Data->exists()) {
            throw new NotFoundException(__('Data inválida'));
        }
        $this->request->allowMethod('post', 'delete');
        if ($this->Data->delete()) {
            $this->Session->setFlash(__('Data borrada exitosamente.'), 'default', array('class' => 'alert alert-success'));
        } else {
            $this->Session->setFlash(__('La data no pudo ser borrada. Por favor, intente de nuevo.'), 'default', array('class' => 'alert alert-danger'));
        }
        return $this->redirect(array('action' => 'index'));
    }

}
