<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {
    
    
    public $components = array(
        //'DebugKit.Toolbar',//plugin para debug
        'Session',
        'Auth' => array(
            'loginRedirect' => array( 'controller' => 'web_users', 'action' => 'index' ),
            'logoutRedirect' => array( 'controller' => 'web_users', 'action' => 'login' ),
            'authorize' => array('Controller'),
            'authError' => 'Debes estar logueado para ver esta pagina.',
            'loginError' => 'Usuario y/o password invalido, por favor intente de nuevo.',
            'authenticate' => array(
                                    'Form' => array(
                                        'scope' => array('activo' => 'S')//activo = S
                                                    )
                                    )
            )
       );
    
    var $helpers = array('Html', 'MainMenu', 'Js', 'Form'/*, 'DataGridder.DataGrid'*/);
    
    
    public function beforeFilter(){
        $this->layout = 'bootstrap';
        
        setlocale(LC_TIME, 'es_ES');
        
        //si el controller no es el default 'users' se debe definir explicitamente
        $this->Auth->loginAction = array('controller'=>'web_users', 'action'=>'login');
        
        $this->Auth->authenticate = array(
            AuthComponent::ALL => array(
                //si el controller no es el default 'users' se debe definir explicitamente
                'userModel' => 'WebUser',
                'fields' => array('username' => 'usuario', 'password' => 'password')
                                        ),
            'Basic',
            'Form'
        );
        
        //permiso para usuarios no logueados
        $this->Auth->allow('login', 'logout', 'home');
        
        /*
        if($this->RequestHandler->responseType() == 'json'){
            $this->RequestHandler->setContent('json', 'application/json' );
        }
        */
    }
    
    
    
    
    public function isAuthorized($user) {
        
        //superadmin		= 1
        if ( isset($user['webuser_profile_id']) && 
                (       //superadmin                            //admin
                    ($user['webuser_profile_id'] === '1')  || ($user['webuser_profile_id'] === '2') 
                ) 
           ) {
            return true;
        }

        // Default deny
        return false;
    }
    
    
    public function redirectFilter(){
	        
        if( AuthComponent::user('webuser_profile_id') === '1' )//superadmin
            return $this->redirect( array('controller' => 'companies', 'action' => 'index') );
        else if( AuthComponent::user('webuser_profile_id') === '2' )//admin
            return $this->redirect( array('controller' => 'challenges', 'action' => 'index') );
        
    }
    
    
    
    public function getSubMenu2($controller, $action = null){
        
        $menu = '';
        
        //'glyphicon-th', 'glyphicon-home', 'glyphicon-stats', 'glyphicon-map-marker', 'glyphicon-user'

        if ( AuthComponent::user('webuser_profile_id')!=NULL && AuthComponent::user('webuser_profile_id')!=''  ){
            
            if( AuthComponent::user('webuser_profile_id') === '1'){//superadmin
                                
                if( $controller=='companies' ){

                    $menu = $this->MainMenu->getMainMenu2(
                        array( 
                            'Data' => array('controller' => 'data', 'action' => 'index'),
                            'Retos' => array('controller' => 'challenges', 'action' => 'index'),
                            'Usuarios Web' => array('controller' => 'web_users', 'action' => 'index')
                        ),
                        array( 
                            'glyphicon-stats', 'glyphicon-home', 'glyphicon-user'
                        ),
                        array('class' => 'submenu'),
                        'class="nav nav-pills nav-stacked"'
                    );

                }else if( $controller=='challenges' ){

                    $menu = $this->MainMenu->getMainMenu2(
                        array( 
                            'Data' => array('controller' => 'data', 'action' => 'index'),
                            'Comercios' => array('controller' => 'companies', 'action' => 'index'),
                            'Usuarios Web' => array('controller' => 'web_users', 'action' => 'index')
                        ),
                        array( 
                            'glyphicon-stats', 'glyphicon-th', 'glyphicon-user'
                        ),
                        array('class' => 'submenu'),
                        'class="nav nav-pills nav-stacked"'
                    );

                }else if( $controller=='web_users' ){

                    $menu = $this->MainMenu->getMainMenu2(
                        array( 
                            'Data' => array('controller' => 'data', 'action' => 'index'),
                            'Comercios' => array('controller' => 'companies', 'action' => 'index'),
                            'Retos' => array('controller' => 'challenges', 'action' => 'index')
                        ),
                        array( 
                            'glyphicon-stats', 'glyphicon-th', 'glyphicon-home'
                        ),
                        array('class' => 'submenu'),
                        'class="nav nav-pills nav-stacked"'
                    );
                    
                }else if( $controller=='data' ){

                    $menu = $this->MainMenu->getMainMenu2(
                        array( 
                            'Comercios' => array('controller' => 'companies', 'action' => 'index'),
                            'Retos' => array('controller' => 'challenges', 'action' => 'index'),
                            'Usuarios Web' => array('controller' => 'web_users', 'action' => 'index')
                        ),
                        array( 
                            'glyphicon-th', 'glyphicon-home', 'glyphicon-user'
                        ),
                        array('class' => 'submenu'),
                        'class="nav nav-pills nav-stacked"'
                    );
                }

            }else if( AuthComponent::user('webuser_profile_id') === '2' ){//admin
                
                /*if( $controller=='companies' ){

                    $menu = $this->MainMenu->getMainMenu2(
                        array( 
                            //'Companies' => array('controller' => 'companies', 'action' => 'index'),
                            'Usuarios Web' => array('controller' => 'web_users', 'action' => 'index')
                        ),
                        array( 
                            'glyphicon-home', 'glyphicon-user'
                        ),
                        array('class' => 'submenu'),
                        'class="nav nav-pills nav-stacked"'
                    );

                }else*/ if( $controller=='challenges' ){

                    $menu = $this->MainMenu->getMainMenu2(
                        array( 
                            'Retos' => array('controller' => 'challenges', 'action' => 'index')
                        ),
                        array( 
                            'glyphicon-home'
                        ),
                        array('class' => 'submenu'),
                        'class="nav nav-pills nav-stacked"'
                    );
                }
            }
        }

        return $menu;
    }
}
