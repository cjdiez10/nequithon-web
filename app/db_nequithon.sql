-- MySQL dump 10.13  Distrib 5.6.24, for osx10.8 (x86_64)
--
-- Host: 192.163.203.146    Database: logimet2_nequithon
-- ------------------------------------------------------
-- Server version	5.6.37

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) NOT NULL,
  `imagen` varchar(100) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (1,'Comidas','http://logimetrica.com/nequithon/img/icon_food.png','2017-12-02 23:45:01'),(2,'Servicios','http://logimetrica.com/nequithon/img/icon_services.png','2017-12-02 23:45:02'),(3,'E-commerce','http://logimetrica.com/nequithon/img/icon_ecommerce.png','2017-12-02 23:45:03'),(4,'Salud','http://logimetrica.com/nequithon/img/icon_health.png','2017-12-02 23:45:04'),(5,'Bebidas','http://logimetrica.com/nequithon/img/icon_drinks.png','2017-12-02 23:45:05'),(6,'Transferencias',NULL,'2017-12-02 23:45:06');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `challenges`
--

DROP TABLE IF EXISTS `challenges`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `challenges` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) NOT NULL,
  `fecha` date NOT NULL,
  `descripcion` varchar(150) NOT NULL,
  `imagen` varchar(45) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `company_id` int(11) NOT NULL,
  `category_id` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `challenges`
--

LOCK TABLES `challenges` WRITE;
/*!40000 ALTER TABLE `challenges` DISABLE KEYS */;
INSERT INTO `challenges` VALUES (1,'Segunda hamburguesa a mitad de precio ','2017-12-03','Disfruta de nuestras hamburguesas 2x1 solo pagando con NEQUI','','2017-12-03 01:40:16',1,'1'),(2,'Reto Condensa - Puntos x2','2017-12-03','Paga tu factura condensa oportunamente y gana el doble de puntos',NULL,'2017-12-03 11:12:28',2,'2'),(3,'Prueba algo delicioso','2017-12-10','Dentro de los siguientes 10 dias come en alguno de los siguientes restaurantes:\r\n- Sierra Nevada\r\n- Don Arturo\r\n- Masa Madre',NULL,'2017-12-03 11:31:35',4,'1'),(5,'SURA - Reto 50 Puntos','2017-12-06','SURA lanza un reto a todos los usuarios de Nequi, para que renueven el SOAT a través de la plataforma Nequi.',NULL,'2017-12-03 14:21:54',3,'2');
/*!40000 ALTER TABLE `challenges` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `companies`
--

DROP TABLE IF EXISTS `companies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `companies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) NOT NULL,
  `webuser_id` int(11) NOT NULL,
  `direccion` varchar(60) DEFAULT NULL,
  `imagen` varchar(100) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `companies`
--

LOCK TABLES `companies` WRITE;
/*!40000 ALTER TABLE `companies` DISABLE KEYS */;
INSERT INTO `companies` VALUES (1,'SIERRA NEVADA - Hamburguesas',3,'Calle 93A # 13A-06. ','https://assets.domicilios.com/img/resources/7549_124_124.jpg','2017-12-02 22:46:01'),(2,'CODENSA - Suba ',4,'Av suba Nº 128 A - 22','https://www.codensa.com.co/static/codensa/img/modules/components/logo_codensa.png','2017-12-03 22:46:01'),(3,'SURA - Seguros',5,'Calle 48 # 27 - 21','http://www.nuestrofondo.co/portals/0/Convenios/SuraLogo.gif','2017-12-03 11:46:01'),(4,'NEQUI',6,'Medellin Headquarter','https://lh3.googleusercontent.com/J2CfzgROe1_weYm7yNIffrAGsGeahADM6r2qMN3C9pNw-i0TJR71LGbVX9y2N7t6dw','2017-12-03 11:46:01');
/*!40000 ALTER TABLE `companies` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `data`
--

DROP TABLE IF EXISTS `data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `telefono` varchar(10) NOT NULL,
  `descripcion` varchar(60) NOT NULL,
  `fecha` date DEFAULT NULL,
  `valor` varchar(12) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=210 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `data`
--

LOCK TABLES `data` WRITE;
/*!40000 ALTER TABLE `data` DISABLE KEYS */;
INSERT INTO `data` VALUES (1,'3003456785','Pago de comidas centro comercial','2017-04-26','10999','2017-12-03 14:26:02'),(2,'3003765684','Pago Servicios públicos TripleA','2017-05-31','30999','2017-12-03 14:26:02'),(3,'3013816354','Jeans para mujer e-commerce','2017-06-15','24999','2017-12-03 14:26:02'),(4,'3029585738','Pago de salud SURA','2017-07-31','54399','2017-12-03 14:26:02'),(5,'3041726254','Pago de comidas centro comercial','2017-07-19','25499','2017-12-03 14:26:02'),(6,'3134846458','Pago Servicios públicos TripleA','2017-08-23','67899','2017-12-03 14:26:02'),(7,'3003914926','Jeans para mujer e-commerce','2017-08-30','32499','2017-12-03 14:26:02'),(8,'3012685866','Pago de salud SURA','2017-09-20','16799','2017-12-03 14:26:02'),(9,'3002586085','Pago de comidas centro comercial','2017-09-12','16899','2017-12-03 14:26:02'),(10,'3135251481','Pago Servicios públicos TripleA','2017-09-28','50999','2017-12-03 14:26:02'),(11,'3264094873','Jeans para mujer e-commerce','2017-10-02','67999','2017-12-03 14:26:02'),(12,'3007006818','Pago de salud SURA','2017-10-19','87999','2017-12-03 14:26:02'),(13,'3128765436','Jeans para mujer e-commerce','2017-11-08','23999','2017-12-03 14:26:02'),(14,'3209873564','Pago de salud SURA','2017-12-01','56999','2017-12-03 14:26:02'),(15,'3003456785','Pago taller automax','2017-04-26','10999','2017-12-03 14:26:02'),(16,'3003456785','Pago matricula colegio santa cruz','2017-04-26','10999','2017-12-03 14:26:02'),(17,'3003765684','Petromil','2017-05-31','30999','2017-12-03 14:26:02'),(18,'3013816354','Botella agua brisa 1 litro','2017-06-15','24999','2017-12-03 14:26:02'),(19,'3029585738','Papita de pollo','2017-07-31','54399','2017-12-03 14:26:02'),(20,'3041726254','Burrito benito juarez','2017-07-19','25499','2017-12-03 14:26:02'),(21,'3134846458','Cafe doble juan valdez','2017-08-23','67899','2017-12-03 14:26:02'),(22,'3003914926','Pastel de oro','2017-08-30','32499','2017-12-03 14:26:02'),(23,'3012685866','Uber X','2017-09-20','16799','2017-12-03 14:26:02'),(24,'3002586085','Rapi','2017-09-12','16899','2017-12-03 14:26:02'),(25,'3135251481','Nexflix','2017-09-28','50999','2017-12-03 14:26:02'),(26,'3264094873','Almacenes Exito','2017-10-02','67999','2017-12-03 14:26:02'),(27,'3007006818','Pollo frito la catedral','2017-10-19','87999','2017-12-03 14:26:02'),(28,'3128765436','Super tiendas Olimpica','2017-11-08','23999','2017-12-03 14:26:02'),(29,'3209873564','Pastel de oro','2017-12-01','56999','2017-12-03 14:26:02'),(30,'3003456785','Pago taller automax','2017-04-26','10999','2017-12-03 14:26:02'),(31,'3003456785','Pago matricula colegio santa cruz','2017-04-26','10999','2017-12-03 14:26:02'),(32,'3003765684','Petromil','2017-05-31','30999','2017-12-03 14:26:02'),(33,'3013816354','Botella agua brisa 1 litro','2017-06-15','24999','2017-12-03 14:26:02'),(34,'3029585738','Papita de pollo','2017-07-31','54399','2017-12-03 14:26:02'),(35,'3041726254','Burrito benito juarez','2017-07-19','25499','2017-12-03 14:26:02'),(36,'3134846458','Cafe doble juan valdez','2017-08-23','67899','2017-12-03 14:26:02'),(37,'3003914926','Pastel de oro','2017-08-30','32499','2017-12-03 14:26:02'),(38,'3012685866','Uber X','2017-09-20','16799','2017-12-03 14:26:02'),(39,'3002586085','Rapi','2017-09-12','16899','2017-12-03 14:26:02'),(40,'3135251481','Nexflix','2017-09-28','50999','2017-12-03 14:26:02'),(41,'3264094873','Almacenes Exito','2017-10-02','67999','2017-12-03 14:26:02'),(42,'3007006818','Pollo frito la catedral','2017-10-19','87999','2017-12-03 14:26:02'),(43,'3128765436','Super tiendas Olimpica','2017-11-08','23999','2017-12-03 14:26:02'),(44,'3209873564','Pastel de oro','2017-12-01','56999','2017-12-03 14:26:02'),(45,'3003456785','Pago taller automax','2017-04-26','10999','2017-12-03 14:26:02'),(46,'3003456785','Pago matricula colegio santa cruz','2017-04-26','10999','2017-12-03 14:26:02'),(47,'3003765684','Petromil','2017-05-31','30999','2017-12-03 14:26:02'),(48,'3013816354','Botella agua brisa 1 litro','2017-06-15','24999','2017-12-03 14:26:02'),(49,'3029585738','Papita de pollo','2017-07-31','54399','2017-12-03 14:26:02'),(50,'3041726254','Burrito benito juarez','2017-07-19','25499','2017-12-03 14:26:02'),(51,'3134846458','Cafe doble juan valdez','2017-08-23','67899','2017-12-03 14:26:02'),(52,'3003914926','Pastel de oro','2017-08-30','32499','2017-12-03 14:26:02'),(53,'3012685866','Uber X','2017-09-20','16799','2017-12-03 14:26:02'),(54,'3002586085','Rapi','2017-09-12','16899','2017-12-03 14:26:02'),(55,'3135251481','Nexflix','2017-09-28','50999','2017-12-03 14:26:02'),(56,'3264094873','Almacenes Exito','2017-10-02','67999','2017-12-03 14:26:02'),(57,'3007006818','Pollo frito la catedral','2017-10-19','87999','2017-12-03 14:26:02'),(58,'3128765436','Super tiendas Olimpica','2017-11-08','23999','2017-12-03 14:26:02'),(59,'3209873564','Pastel de oro','2017-12-01','56999','2017-12-03 14:26:02'),(60,'3003456785','Pago taller automax','2017-04-26','10999','2017-12-03 14:26:02'),(61,'3003456785','Pago matricula colegio santa cruz','2017-04-26','10999','2017-12-03 14:26:02'),(62,'3003765684','Petromil','2017-05-31','30999','2017-12-03 14:26:02'),(63,'3013816354','Botella agua brisa 1 litro','2017-06-15','24999','2017-12-03 14:26:02'),(64,'3029585738','Papita de pollo','2017-07-31','54399','2017-12-03 14:26:02'),(65,'3041726254','Burrito benito juarez','2017-07-19','25499','2017-12-03 14:26:02'),(66,'3134846458','Cafe doble juan valdez','2017-08-23','67899','2017-12-03 14:26:02'),(67,'3003914926','Pastel de oro','2017-08-30','32499','2017-12-03 14:26:02'),(68,'3012685866','Uber X','2017-09-20','16799','2017-12-03 14:26:02'),(69,'3002586085','Rapi','2017-09-12','16899','2017-12-03 14:26:02'),(70,'3135251481','Nexflix','2017-09-28','50999','2017-12-03 14:26:02'),(71,'3264094873','Almacenes Exito','2017-10-02','67999','2017-12-03 14:26:02'),(72,'3007006818','Pollo frito la catedral','2017-10-19','87999','2017-12-03 14:26:02'),(73,'3128765436','Super tiendas Olimpica','2017-11-08','23999','2017-12-03 14:26:02'),(74,'3209873564','Pastel de oro','2017-12-01','56999','2017-12-03 14:26:02'),(75,'3003456785','Pago taller automax','2017-04-26','10999','2017-12-03 14:26:02'),(76,'3003456785','Pago matricula colegio santa cruz','2017-04-26','10999','2017-12-03 14:26:02'),(77,'3003765684','Petromil','2017-05-31','30999','2017-12-03 14:26:02'),(78,'3013816354','Botella agua brisa 1 litro','2017-06-15','24999','2017-12-03 14:26:02'),(79,'3029585738','Papita de pollo','2017-07-31','54399','2017-12-03 14:26:02'),(80,'3041726254','Burrito benito juarez','2017-07-19','25499','2017-12-03 14:26:02'),(81,'3134846458','Cafe doble juan valdez','2017-08-23','67899','2017-12-03 14:26:02'),(82,'3003914926','Pastel de oro','2017-08-30','32499','2017-12-03 14:26:02'),(83,'3012685866','Uber X','2017-09-20','16799','2017-12-03 14:26:02'),(84,'3002586085','Rapi','2017-09-12','16899','2017-12-03 14:26:02'),(85,'3135251481','Nexflix','2017-09-28','50999','2017-12-03 14:26:02'),(86,'3264094873','Almacenes Exito','2017-10-02','67999','2017-12-03 14:26:02'),(87,'3007006818','Pollo frito la catedral','2017-10-19','87999','2017-12-03 14:26:02'),(88,'3128765436','Super tiendas Olimpica','2017-11-08','23999','2017-12-03 14:26:02'),(89,'3209873564','Pastel de oro','2017-12-01','56999','2017-12-03 14:26:02'),(90,'3003456785','Pago taller automax','2017-04-26','10999','2017-12-03 14:26:02'),(91,'3003456785','Pago matricula colegio santa cruz','2017-04-26','10999','2017-12-03 14:26:02'),(92,'3003765684','Petromil','2017-05-31','30999','2017-12-03 14:26:02'),(93,'3013816354','Botella agua brisa 1 litro','2017-06-15','24999','2017-12-03 14:26:02'),(94,'3029585738','Papita de pollo','2017-07-31','54399','2017-12-03 14:26:02'),(95,'3041726254','Burrito benito juarez','2017-07-19','25499','2017-12-03 14:26:02'),(96,'3134846458','Cafe doble juan valdez','2017-08-23','67899','2017-12-03 14:26:02'),(97,'3003914926','Pastel de oro','2017-08-30','32499','2017-12-03 14:26:02'),(98,'3012685866','Uber X','2017-09-20','16799','2017-12-03 14:26:02'),(99,'3002586085','Rapi','2017-09-12','16899','2017-12-03 14:26:02'),(100,'3135251481','Nexflix','2017-09-28','50999','2017-12-03 14:26:02'),(101,'3264094873','Almacenes Exito','2017-10-02','67999','2017-12-03 14:26:02'),(102,'3007006818','Pollo frito la catedral','2017-10-19','87999','2017-12-03 14:26:02'),(103,'3128765436','Super tiendas Olimpica','2017-11-08','23999','2017-12-03 14:26:02'),(104,'3209873564','Pastel de oro','2017-12-01','56999','2017-12-03 14:26:02'),(105,'3003456785','Pago taller automax','2017-04-26','10999','2017-12-03 14:26:02'),(106,'3003456785','Pago matricula colegio santa cruz','2017-04-26','10999','2017-12-03 14:26:02'),(107,'3003765684','Petromil','2017-05-31','30999','2017-12-03 14:26:02'),(108,'3013816354','Botella agua brisa 1 litro','2017-06-15','24999','2017-12-03 14:26:02'),(109,'3029585738','Papita de pollo','2017-07-31','54399','2017-12-03 14:26:02'),(110,'3041726254','Burrito benito juarez','2017-07-19','25499','2017-12-03 14:26:02'),(111,'3134846458','Cafe doble juan valdez','2017-08-23','67899','2017-12-03 14:26:02'),(112,'3003914926','Pastel de oro','2017-08-30','32499','2017-12-03 14:26:02'),(113,'3012685866','Uber X','2017-09-20','16799','2017-12-03 14:26:02'),(114,'3002586085','Rapi','2017-09-12','16899','2017-12-03 14:26:02'),(115,'3135251481','Nexflix','2017-09-28','50999','2017-12-03 14:26:02'),(116,'3264094873','Almacenes Exito','2017-10-02','67999','2017-12-03 14:26:02'),(117,'3007006818','Pollo frito la catedral','2017-10-19','87999','2017-12-03 14:26:02'),(118,'3128765436','Super tiendas Olimpica','2017-11-08','23999','2017-12-03 14:26:02'),(119,'3209873564','Pastel de oro','2017-12-01','56999','2017-12-03 14:26:02'),(120,'3003456785','Pago taller automax','2017-04-26','10999','2017-12-03 14:26:02'),(121,'3003456785','Pago matricula colegio santa cruz','2017-04-26','10999','2017-12-03 14:26:02'),(122,'3003765684','Petromil','2017-05-31','30999','2017-12-03 14:26:02'),(123,'3013816354','Botella agua brisa 1 litro','2017-06-15','24999','2017-12-03 14:26:02'),(124,'3029585738','Papita de pollo','2017-07-31','54399','2017-12-03 14:26:02'),(125,'3041726254','Burrito benito juarez','2017-07-19','25499','2017-12-03 14:26:02'),(126,'3134846458','Cafe doble juan valdez','2017-08-23','67899','2017-12-03 14:26:02'),(127,'3003914926','Pastel de oro','2017-08-30','32499','2017-12-03 14:26:02'),(128,'3012685866','Uber X','2017-09-20','16799','2017-12-03 14:26:02'),(129,'3002586085','Rapi','2017-09-12','16899','2017-12-03 14:26:02'),(130,'3135251481','Nexflix','2017-09-28','50999','2017-12-03 14:26:02'),(131,'3264094873','Almacenes Exito','2017-10-02','67999','2017-12-03 14:26:02'),(132,'3007006818','Pollo frito la catedral','2017-10-19','87999','2017-12-03 14:26:02'),(133,'3128765436','Super tiendas Olimpica','2017-11-08','23999','2017-12-03 14:26:02'),(134,'3209873564','Pastel de oro','2017-12-01','56999','2017-12-03 14:26:02'),(135,'3003456785','Pago taller automax','2017-04-26','10999','2017-12-03 14:26:02'),(136,'3003456785','Pago matricula colegio santa cruz','2017-04-26','10999','2017-12-03 14:26:02'),(137,'3003765684','Petromil','2017-05-31','30999','2017-12-03 14:26:02'),(138,'3013816354','Botella agua brisa 1 litro','2017-06-15','24999','2017-12-03 14:26:02'),(139,'3029585738','Papita de pollo','2017-07-31','54399','2017-12-03 14:26:02'),(140,'3041726254','Burrito benito juarez','2017-07-19','25499','2017-12-03 14:26:02'),(141,'3134846458','Cafe doble juan valdez','2017-08-23','67899','2017-12-03 14:26:02'),(142,'3003914926','Pastel de oro','2017-08-30','32499','2017-12-03 14:26:02'),(143,'3012685866','Uber X','2017-09-20','16799','2017-12-03 14:26:02'),(144,'3002586085','Rapi','2017-09-12','16899','2017-12-03 14:26:02'),(145,'3135251481','Nexflix','2017-09-28','50999','2017-12-03 14:26:02'),(146,'3264094873','Almacenes Exito','2017-10-02','67999','2017-12-03 14:26:02'),(147,'3007006818','Pollo frito la catedral','2017-10-19','87999','2017-12-03 14:26:02'),(148,'3128765436','Super tiendas Olimpica','2017-11-08','23999','2017-12-03 14:26:02'),(149,'3209873564','Pastel de oro','2017-12-01','56999','2017-12-03 14:26:02'),(150,'3003456785','Pago taller automax','2017-04-26','10999','2017-12-03 14:26:02'),(151,'3003456785','Pago matricula colegio santa cruz','2017-04-26','10999','2017-12-03 14:26:02'),(152,'3003765684','Petromil','2017-05-31','30999','2017-12-03 14:26:02'),(153,'3013816354','Botella agua brisa 1 litro','2017-06-15','24999','2017-12-03 14:26:02'),(154,'3029585738','Papita de pollo','2017-07-31','54399','2017-12-03 14:26:02'),(155,'3041726254','Burrito benito juarez','2017-07-19','25499','2017-12-03 14:26:02'),(156,'3134846458','Cafe doble juan valdez','2017-08-23','67899','2017-12-03 14:26:02'),(157,'3003914926','Pastel de oro','2017-08-30','32499','2017-12-03 14:26:02'),(158,'3012685866','Uber X','2017-09-20','16799','2017-12-03 14:26:02'),(159,'3002586085','Rapi','2017-09-12','16899','2017-12-03 14:26:02'),(160,'3135251481','Nexflix','2017-09-28','50999','2017-12-03 14:26:02'),(161,'3264094873','Almacenes Exito','2017-10-02','67999','2017-12-03 14:26:02'),(162,'3007006818','Pollo frito la catedral','2017-10-19','87999','2017-12-03 14:26:02'),(163,'3128765436','Super tiendas Olimpica','2017-11-08','23999','2017-12-03 14:26:02'),(164,'3209873564','Pastel de oro','2017-12-01','56999','2017-12-03 14:26:02'),(165,'3003456785','Pago taller automax','2017-04-26','10999','2017-12-03 14:26:02'),(166,'3003456785','Pago matricula colegio santa cruz','2017-04-26','10999','2017-12-03 14:26:02'),(167,'3003765684','Petromil','2017-05-31','30999','2017-12-03 14:26:02'),(168,'3013816354','Botella agua brisa 1 litro','2017-06-15','24999','2017-12-03 14:26:02'),(169,'3029585738','Papita de pollo','2017-07-31','54399','2017-12-03 14:26:02'),(170,'3041726254','Burrito benito juarez','2017-07-19','25499','2017-12-03 14:26:02'),(171,'3134846458','Cafe doble juan valdez','2017-08-23','67899','2017-12-03 14:26:02'),(172,'3003914926','Pastel de oro','2017-08-30','32499','2017-12-03 14:26:02'),(173,'3012685866','Uber X','2017-09-20','16799','2017-12-03 14:26:02'),(174,'3002586085','Rapi','2017-09-12','16899','2017-12-03 14:26:02'),(175,'3135251481','Nexflix','2017-09-28','50999','2017-12-03 14:26:02'),(176,'3264094873','Almacenes Exito','2017-10-02','67999','2017-12-03 14:26:02'),(177,'3007006818','Pollo frito la catedral','2017-10-19','87999','2017-12-03 14:26:02'),(178,'3128765436','Super tiendas Olimpica','2017-11-08','23999','2017-12-03 14:26:02'),(179,'3209873564','Pastel de oro','2017-12-01','56999','2017-12-03 14:26:02'),(180,'3003456785','Pago taller automax','2017-04-26','10999','2017-12-03 14:26:02'),(181,'3003456785','Pago matricula colegio santa cruz','2017-04-26','10999','2017-12-03 14:26:02'),(182,'3003765684','Petromil','2017-05-31','30999','2017-12-03 14:26:02'),(183,'3013816354','Botella agua brisa 1 litro','2017-06-15','24999','2017-12-03 14:26:02'),(184,'3029585738','Papita de pollo','2017-07-31','54399','2017-12-03 14:26:02'),(185,'3041726254','Burrito benito juarez','2017-07-19','25499','2017-12-03 14:26:02'),(186,'3134846458','Cafe doble juan valdez','2017-08-23','67899','2017-12-03 14:26:02'),(187,'3003914926','Pastel de oro','2017-08-30','32499','2017-12-03 14:26:02'),(188,'3012685866','Uber X','2017-09-20','16799','2017-12-03 14:26:02'),(189,'3002586085','Rapi','2017-09-12','16899','2017-12-03 14:26:02'),(190,'3135251481','Nexflix','2017-09-28','50999','2017-12-03 14:26:02'),(191,'3264094873','Almacenes Exito','2017-10-02','67999','2017-12-03 14:26:02'),(192,'3007006818','Pollo frito la catedral','2017-10-19','87999','2017-12-03 14:26:02'),(193,'3128765436','Super tiendas Olimpica','2017-11-08','23999','2017-12-03 14:26:02'),(194,'3209873564','Pastel de oro','2017-12-01','56999','2017-12-03 14:26:02'),(195,'3003456785','Pago taller automax','2017-04-26','10999','2017-12-03 14:26:02'),(196,'3003456785','Pago matricula colegio santa cruz','2017-04-26','10999','2017-12-03 14:26:02'),(197,'3003765684','Petromil','2017-05-31','30999','2017-12-03 14:26:02'),(198,'3013816354','Botella agua brisa 1 litro','2017-06-15','24999','2017-12-03 14:26:02'),(199,'3029585738','Papita de pollo','2017-07-31','54399','2017-12-03 14:26:02'),(200,'3041726254','Burrito benito juarez','2017-07-19','25499','2017-12-03 14:26:02'),(201,'3134846458','Cafe doble juan valdez','2017-08-23','67899','2017-12-03 14:26:02'),(202,'3003914926','Pastel de oro','2017-08-30','32499','2017-12-03 14:26:02'),(203,'3012685866','Uber X','2017-09-20','16799','2017-12-03 14:26:02'),(204,'3002586085','Rapi','2017-09-12','16899','2017-12-03 14:26:02'),(205,'3135251481','Nexflix','2017-09-28','50999','2017-12-03 14:26:02'),(206,'3264094873','Almacenes Exito','2017-10-02','67999','2017-12-03 14:26:02'),(207,'3007006818','Pollo frito la catedral','2017-10-19','87999','2017-12-03 14:26:02'),(208,'3128765436','Super tiendas Olimpica','2017-11-08','23999','2017-12-03 14:26:02'),(209,'3209873564','Pastel de oro','2017-12-01','56999','2017-12-03 14:26:02');
/*!40000 ALTER TABLE `data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mobile_users`
--

DROP TABLE IF EXISTS `mobile_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mobile_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) NOT NULL,
  `celular` varchar(10) NOT NULL,
  `activo` varchar(1) DEFAULT NULL COMMENT 'S = Si, N = No',
  `foto` varchar(60) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mobile_users`
--

LOCK TABLES `mobile_users` WRITE;
/*!40000 ALTER TABLE `mobile_users` DISABLE KEYS */;
INSERT INTO `mobile_users` VALUES (1,'Cristian Díez','3012685866','S',NULL,'2017-12-03 14:49:01');
/*!40000 ALTER TABLE `mobile_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `web_users`
--

DROP TABLE IF EXISTS `web_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `web_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(80) DEFAULT NULL,
  `usuario` varchar(45) NOT NULL,
  `password` varchar(100) NOT NULL,
  `copiapassword` varchar(50) NOT NULL,
  `webuser_profile_id` int(11) NOT NULL,
  `district_id` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `activo` char(1) DEFAULT 'S' COMMENT 'S = si\nN = no',
  `company_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `web_users`
--

LOCK TABLES `web_users` WRITE;
/*!40000 ALTER TABLE `web_users` DISABLE KEYS */;
INSERT INTO `web_users` VALUES (1,'Super Admin Ciudad-Origen','corigen@gmail.com','c45bc6974bbef115e53042bf9078851ea0c8fce5','123',1,0,'2017-05-13 17:30:00',NULL,'S',0),(3,'Admin Sierra Nevada 93','snevada93@gmail.com','d2d5120aa56fd5929bbe25d715a6cf1e1e6c5442','123456',2,1,'2017-10-31 10:04:15','2017-10-31 10:04:15','S',1),(4,'Admin Codensa Suba','codensasuba@gmail.com','d2d5120aa56fd5929bbe25d715a6cf1e1e6c5442','123456',2,0,'2017-12-03 11:04:15',NULL,'S',2),(5,'Admin Seguros Sura','ssura@gmail.com','d2d5120aa56fd5929bbe25d715a6cf1e1e6c5442','123456',2,0,'2017-12-03 11:04:15',NULL,'S',3),(6,'Admin Nequi','nequi@gmail.com','d2d5120aa56fd5929bbe25d715a6cf1e1e6c5442','123456',2,NULL,'2017-12-03 11:04:15',NULL,'S',4);
/*!40000 ALTER TABLE `web_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `webuser_profiles`
--

DROP TABLE IF EXISTS `webuser_profiles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `webuser_profiles` (
  `id` int(11) NOT NULL DEFAULT '0',
  `nombre` varchar(20) NOT NULL,
  `descripcion` varchar(100) NOT NULL,
  `created` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `webuser_profiles`
--

LOCK TABLES `webuser_profiles` WRITE;
/*!40000 ALTER TABLE `webuser_profiles` DISABLE KEYS */;
INSERT INTO `webuser_profiles` VALUES (1,'SuperAdmin','Super administrador, todos los privilegios','2017-05-13 17:00:00'),(2,'Admin','Administrador a nivel nacional','2017-05-13 17:01:00');
/*!40000 ALTER TABLE `webuser_profiles` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-12-03 16:05:12
