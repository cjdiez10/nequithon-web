<?php


function time_elapsed_B($secs) {

    $bit = array(
        ' year' => $secs / 31556926 % 12,
        ' week' => $secs / 604800 % 52,
        ' day' => $secs / 86400 % 7,
        ' hour' => $secs / 3600 % 24,
        ' minute' => $secs / 60 % 60,
        ' second' => $secs % 60
    );

    foreach ($bit as $k => $v) {
        if ($v > 1)
            $ret[] = $v . $k . 's';
        if ($v == 1)
            $ret[] = $v . $k;
    }
    array_splice($ret, count($ret) - 1, 0, 'and');
    //$ret[] = 'ago.';

    return join(' ', $ret);
}

function time_elapsed_A($secs) {
    $bit = array(
        'y' => $secs / 31556926 % 12,
        'w' => $secs / 604800 % 52,
        'd' => $secs / 86400 % 7,
        'h' => $secs / 3600 % 24,
        'm' => $secs / 60 % 60,
        's' => $secs % 60
    );

    foreach ($bit as $k => $v)
        if ($v > 0)
            $ret[] = $v . $k;

    return join(' ', $ret);
}

/**
 * Convert excel file to json
 * @param type $file
 * @return type json convertion
 */
function excel2Json($file) {

    require_once(APP . 'Vendor' . DS . 'PHPExcel/PHPExcel/IOFactory.php');

    $fileObj = PHPExcel_IOFactory::load($file);
    //$sheetObj = $fileObj->getActiveSheet();
    
    //echo '<br/><br/><br/><br/>'.$file.'<br/>'.json_encode($fileObj);

    //$highestColumm = $fileObj->setActiveSheetIndex(0)->getHighestColumn();
    $highestRow = $fileObj->setActiveSheetIndex(0)->getHighestRow();

    for ($row = 1; $row < $highestRow + 1; $row++) {
        $dataset = array();
        
        $column = 0;
        $columnValue = $columnValue = $fileObj->setActiveSheetIndex(0)->getCellByColumnAndRow($column, $row)->getFormattedValue();
        while ($columnValue != NULL) {
            $dataset[] = $columnValue;
            $column++;
            $columnValue = $fileObj->setActiveSheetIndex(0)->getCellByColumnAndRow($column, $row)->getFormattedValue();
        }
        
        $datasets[] = $dataset;
    }
    
    //echo '<br/><br/><br/><br/>'.count($datasets);// json_encode($datasets);

    $header = $datasets[0];

    $jsonData = array();
    $blocco = array();

    for ($i = 1; $i < count($datasets); $i++) {
        
        if( $datasets[$i]!=NULL && count($datasets[$i])>0 ){

            $parte = array();
            $parte = $datasets[$i];

            $j = 0;
            foreach ($header as $col) {
                $blocco[$col] = $parte[$j];
                $j++;
            }
            $jsonData[] = $blocco;

            unset($parte);
        }
    }
    
    //echo '<br/><br/><br/>'.count($jsonData);//.'<br/>'.json_encode($jsonData);
    
    //return json_encode($jsonData);
    return $jsonData;
}

function httpConnectionToJson($url, $type, $headers, $params){
    
    $json = NULL;
    
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_URL, $url);
    
    if( $headers!=NULL )
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    
    if( $type=="POST" ){
     
        curl_setopt($ch, CURLOPT_POST, 1);
        
        if( $params!=NULL )
            curl_setopt( $ch, CURLOPT_POSTFIELDS, http_build_query($params) );
    }

    $result = curl_exec($ch);
    curl_close($ch);
    
    
    if( $type=="POST" )
        $json = json_decode($result, true);
    else if( $type=="POST" )
        $json = json_decode($result);
    
    
    return $json;
}

?>